﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu()]
public class ARContentLink : ScriptableObject {
	public ARTarget target;
	public GameObject contentPrefab; // only 1 prefab for now.

}
