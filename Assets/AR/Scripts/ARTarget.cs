﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu()]
public class ARTarget : ScriptableObject {
	public string ID;
	public GameObject prefab;
}
