﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;


[CustomEditor(typeof(SplineObjectPlacement))]
public class SplineObjectPlacementEditor : Editor {

	public override void OnInspectorGUI()
	{
		DrawDefaultInspector();
		if (GUILayout.Button("Place Objects")) {
			SplineObjectPlacement spline = (SplineObjectPlacement)target;
			spline.PlaceObjects();
		}
		if (GUILayout.Button("Clear Objects")) {
			SplineObjectPlacement spline = (SplineObjectPlacement)target;
			spline.ClearObjects();
		}
	}
}
