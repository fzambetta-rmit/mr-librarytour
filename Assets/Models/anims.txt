
   1 -   	bind pose
   2 -   38	greeting wave
  40 -   88	point left
  90 -  138	point right
 140		pose - yay
 141		pose - flight
 145 -  205	idle, float
 206 -  326	idle, look side to side
 327 -  400	idle, flap
 401 -  450	question (can I help you)
 451 -  534	prop - clock, opening hours
 535 -  635	prop - help desk
 636 -  770	prop - laptop, finding computers
 771 -  879	prop - group work, mini owls
 880 - 1050	prop - where are the books?
1051 - 1200	prop - private study, reading book
1105 - 1171	-- reading book loop
1201 - 1300	prop - toilets
1301 - 1433	prop - group study areas
1434 - 1631	prop - printing
1632 - 1755	prop - drinking fountain
1756 - 1816	prop - returns chute
1900 - 1938	generic - nodding
1940 - 1995	generic - shake head
1996 - 2050	generic - thinking, head tilt down, hmm
2051 - 2106	hololens - on
2107 - 2143	hololens - wave
2144 - 2185	hololens - off


pending
xxxx - xxxx	generic - wobble on spot, happy informing
xxxx - xxxx	generic - head tilt side
xxxx - xxxx	special - bow tie spin, happy