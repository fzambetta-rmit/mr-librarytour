﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OwlBotPosterLocator : MonoBehaviour {

    public PosterInfo poster;

	// Use this for initialization
	void Awake () {
		if (poster != null)
        {
            if (poster.OwlBotLocation != null)
            {
                Debug.LogWarning("Warning! Replacing poster locator possible duplication.");
            }
            else{
                poster.OwlBotLocation = this;
            }
        }
	}
}
