﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class PlatformSceneLoader : MonoBehaviour {

    [Header("Platform Scenes")]
    public string HololensPlatformSceneName;
    public string MobilePlatformSceneName;

    public Vuforia.VuforiaConfiguration vuforiaConfiguration;
    void Awake()
    {
        LoadCorrectScene();

    }

    void LoadCorrectScene()
    {
		// Hololens
		if (UnityEngine.XR.XRDevice.isPresent)
		{
			SceneManager.LoadScene(HololensPlatformSceneName);
		}
		else
		{
			SceneManager.LoadScene(MobilePlatformSceneName);
		}
	}
}
