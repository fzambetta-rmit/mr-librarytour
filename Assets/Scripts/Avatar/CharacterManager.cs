﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using TMPro;
using RIGLabs.EventManager;

public class CharacterManager : MonoBehaviour
{
    public Animator myAnimator;
    public SpeechBubble speechBubble;
    public GameObject myCanvas, hexButtons;
    //   public GameObject[] trails;
    public TextMeshProUGUI buttonText;
    public AudioClip[] audioClips;
    public AudioSource owlbotAudio;
    public GameObject transporterEffect;
    public Transform transporterLocation;

    public GameObject backButton;
    public GameObject tutorialPanel;

    public TimeandLocation timeandlocation;
	public GameObject solarSystem, beachScene;

    public struct ScriptElement
    {
        public string title;
        public string body;
        public string url;
        public Platforms platform;
    }

    public bool playAudio;
    [Tooltip("When true the app progresses without any user input")]
    public bool automaticProgress;
    public float autoProgressDelay;
    public static bool owlActive = false;

    public enum Platforms
    {
        all,
        mobile,
        hololens
    };

    private int scriptIndex;
    private string dataFilename = "interactionScript.json"; // relative to /StreamingAssets
    private PosterInfo currentPoster = null;
    private DialogueLine currentLine = null;
    private Stack<DialogueLine> dialogueLineHistory = new Stack<DialogueLine>();
    public enum Gesture { none, wave, pointLeft, pointRight, question, helpDesk, groupWork, laptop, openingHours, idleFlap, idleLook, whereAreTheBooks, returningBooks, readingBook, toilets, groupStudy, printing, drinkingFountain, returnChute, nodding, shakeHead, thinking, hololens };

    private ScriptElement[] interactionScript;
    private const int scriptLength = 29;
    private List<int> posterContent; // which interaction script entries are needed for this specific poster
    public int posterNumber;

    public static Platforms currentPlatform = Platforms.hololens; // TODO: read this from the hardware
    private void Awake()
    {
        if (timeandlocation == null)
        {
            timeandlocation = FindObjectOfType<TimeandLocation>();
        }
    }
    void Start()
    {
		currentPlatform = Platforms.hololens;
		solarSystem.SetActive(false);
		beachScene.SetActive(false);
    }

    public void Initialize(int _posterNumber)
    {
        posterNumber = _posterNumber;
        // fill script interactionScript
        interactionScript = new ScriptElement[scriptLength];
        posterContent = new List<int>();

        for (int i = 0; i < scriptLength; i++)
        {
            interactionScript[1].url = ""; // initialize URLs to blank, they're only shown on mobile
        }

        Debug.Log(posterContent.Count.ToString() + " elements in poster content, starting with number " + posterContent[0].ToString());

        //myCanvas.SetActive(true);
        hexButtons.SetActive(false);
        scriptIndex = 0;
        DisplayContent();
    }

    private void AvatarGesture(Gesture gesture)
    {
        myAnimator.SetTrigger(gesture.ToString());
    }

    #region New Interface
    public void PlayAnimation(DialogueLine line)
    {
        if (line.gestures.Count > 0 && line.gestures[0] != Gesture.none)
            AvatarGesture(line.gestures[0]);
    }

    public bool ShowPoster(PosterInfo poster)
    {
        if (poster == currentPoster)
        {
            return false;
        }
        if (poster.order == 1)
        {
            tutorialPanel.SetActive(true);
            TutorialPanel.hasBeenTriggered = true;

        }
        EventManager.TriggerEvent<PosterInfo>(Events.PosterDetected, poster);
        Show();
        timeandlocation.owlbotactive();
        currentPoster = poster;
        currentLine = GetNextLine(poster);
        dialogueLineHistory.Clear();
        UpdateBackButton();
        ShowDisplaySpeechBubble(currentLine);
        
        // draw transporter effect
        // it's parented to owlbot so that it moves when owlbot is moved in the ShowPoster() method below
        Instantiate(transporterEffect, transporterLocation.position, Quaternion.identity, transporterLocation);
        timeandlocation.displayReturnAnimation(currentPoster);

		// turn on solar system model in the poster before the Starwars room so it's visible on the walk towards the room. Turn it off on the next poster
		if (poster.order == 8)
			solarSystem.SetActive(true);
		if (poster.order == 10)
			solarSystem.SetActive(false);

		// turn on beach scene
		if (poster.order == 9) // starwars group study area
			beachScene.SetActive(true);
		if (poster.order == 11) // book a room
			beachScene.SetActive(false);

        return true;
    }

    public void ShowPoster(PosterInfo poster, Transform vumarkTransform)
    {
        if (ShowPoster(poster))
        {
            transform.position = vumarkTransform.position;
            transform.up = Vector3.up;
        }
    }

    private DialogueLine GetNextLine(PosterInfo poster, DialogueLine current = null)
    {
        int index = 0;

        if (current != null)
            index = poster.Lines.IndexOf(current);
        else
            index = -1;

        index++;

        // If the index or is the last line just short out.
        if (index >= poster.Lines.Count)
            return null;

        // Check Platform Specifics
        do
        {
            if (poster.Lines[index].Platform == Platforms.all ||
                poster.Lines[index].Platform == currentPlatform)
            {
                return poster.Lines[index];
            }
            index++;
        }
        while (index < poster.Lines.Count);

        return null;
    }

    public void ShowDisplaySpeechBubble(DialogueLine line)
    {
        if (line == null)
            return;

        speechBubble.DrawSpeechBubble(line.TitleText, line.BodyText);

        PlayAnimation(line);
    }

    public void Hide()
    {
        gameObject.SetActive(false);
        currentPoster = null;
        currentLine = null;
        owlActive = false;
    }

    public void Show()
    {
        owlActive = true;
        gameObject.SetActive(true);

		// check if there's a left-over transporter effect
		// this can happen if the player skips through the dialog before the transporter has finished its animation, and it doesn't have time to destroy itself
		if (transporterLocation.childCount > 0) {
			Destroy(transporterLocation.GetChild(0).gameObject);
		}

    }

    public void Init()
    {
        Hide();
    }

    private bool ContinueScript()
    {
        dialogueLineHistory.Push(currentLine);
        currentLine = GetNextLine(currentPoster, currentLine);
        if (currentLine != null)
        {
            ShowDisplaySpeechBubble(currentLine);
            return true;
        }
        else
        {
            return false;
        }
    }

    #endregion
    /// <summary>
    /// Displays the next element from the poster list (using the scriptIndex as a counter)
    /// </summary>
    private void DisplayContent()
    {
        Debug.Log("Script index: " + scriptIndex.ToString() + ", displaying script element " + posterContent[scriptIndex].ToString());

        // draw the speech bubble with the interactionScript
        int nextItem = posterContent[scriptIndex];
        speechBubble.DrawSpeechBubble(interactionScript[nextItem].title, interactionScript[nextItem].body);

        return;
    }

    public void BackButtonPressed()
    {
        if (dialogueLineHistory.Count == 0)
        {
            return;
        }

        if (playAudio)
        {
            owlbotAudio.clip = audioClips[0]; // click
            owlbotAudio.Play();
        }

        currentLine = dialogueLineHistory.Pop();
        ShowDisplaySpeechBubble(currentLine);
        UpdateBackButton();
    }

    void UpdateBackButton()
    {
        backButton.SetActive(dialogueLineHistory.Count > 0);
    }

    public void PressedButton(int buttonNumber)
    {
        if (tutorialPanel.activeSelf == true)
        {
            tutorialPanel.SetActive(false);
            TutorialPanel.hasBeenTriggered = true;
            timeandlocation.StartCoundownTimer();
        }

        if (playAudio)
        {
            owlbotAudio.clip = audioClips[0]; // click
            owlbotAudio.Play();
        }
        if (ContinueScript() == false)
        {
            EventManager.TriggerEvent<PosterInfo>(Events.DialogueFinished, currentPoster);
            int nextPosterIndex = 1;
            // teleport Owlbot away
            switch (timeandlocation.numberposters)
            {
                case 1:
                    timeandlocation.trails[0].SetActive(true);
                    nextPosterIndex = 2;
                    break;
                case 2:
                    timeandlocation.trails[1].SetActive(true);
                    nextPosterIndex = 3;
                    break;
                case 3:
                    timeandlocation.trails[2].SetActive(true);
                    nextPosterIndex = 4;
                    break;
                case 4:
                    timeandlocation.trails[3].SetActive(true);
                    nextPosterIndex = 5;
                    break;
                case 5:
                    timeandlocation.trails[4].SetActive(true);
                    timeandlocation.trails[5].SetActive(true);
                    nextPosterIndex = 6;
                    break;
                case 6:
                    if (timeandlocation.seenPosters.Contains(timeandlocation.ReserveCollectionPoster))
                    {
                        timeandlocation.trails[7].SetActive(true);
                        nextPosterIndex = 8;
                    }
                    else
                    {
                        timeandlocation.trails[6].SetActive(true);
                        nextPosterIndex = 7;
                    }
                    break;
                case 7:
                    if (timeandlocation.seenPosters.Contains(timeandlocation.SilentStudyPoster))
                    {
                        timeandlocation.trails[8].SetActive(true);
                        nextPosterIndex = 8;
                    }
                    else
                    {
                        timeandlocation.trails[6].SetActive(true);
                        nextPosterIndex = 6;
                    }
                    break;
                case 8:
                    timeandlocation.trails[9].SetActive(true);
                    timeandlocation.trails[10].SetActive(true);
                    nextPosterIndex = 9;
                    break;
                case 9:

                    timeandlocation.trails[11].SetActive(true);
                    timeandlocation.trails[12].SetActive(true);
                    timeandlocation.trails[13].SetActive(true);
                    nextPosterIndex = 10;
                    break;
                case 10:
                    timeandlocation.trails[14].SetActive(true);
                    timeandlocation.trails[15].SetActive(true);
                    nextPosterIndex = 11;
                    break;
                case 11:
                    timeandlocation.trails[16].SetActive(true);
                    nextPosterIndex = 12;
                    break;
            }
            Hide();
            timeandlocation.owlbotnotactive();
            PosterInfo info = ContentManager.Instance.GetPosterFromOrder(nextPosterIndex);
            EventManager.TriggerEvent<PosterInfo>(Events.NewPosterTarget, info);
        }
        UpdateBackButton();
    }
    public void OnResetCall()
    {
        TutorialPanel.hasBeenTriggered = false;
    }
}
