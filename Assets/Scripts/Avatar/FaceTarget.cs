﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FaceTarget : MonoBehaviour {

	public Transform target;
	public bool flip180;

	private Transform myTransform;

	private void Start()
	{
		myTransform = transform;

		// flip child 180 to correct look-at direction
		if (flip180)
			myTransform.GetChild(0).Rotate(0, 180, 0);
	}

	void Update () {
		Vector3 targetVector = target.position;
		targetVector.y = myTransform.position.y;
		myTransform.LookAt(targetVector);	
	}
}
