﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Light))]

public class FlickerLight : MonoBehaviour {

	public float maxIntensity, flickerRate;
	private Light myLight;

	void Start () {
		myLight = GetComponent<Light>();		
	}
	
	void Update () {
		myLight.intensity = Mathf.PerlinNoise(Time.time * flickerRate, 0) * maxIntensity;		
	}
}
