﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class HexButtons : MonoBehaviour {

	[Tooltip("Button 0 is the large question hex")]
	public GameObject[] buttons;
	public TextMeshPro[] labels;

	void Start () {
		foreach (GameObject button in buttons) {
			button.SetActive(false);
		}
	}
	
	/// <summary>
	/// Draw hex buttons with the provided content
	/// </summary>
	/// <param name="content">First element is the question, the rest are the answers.
	/// Max six answers.</param>
	public void DrawButtons (string[] content)
	{
		for (int i = 0; i < buttons.Length; i++) {
			if (i < content.Length) {
				labels[i].text = content[i];
				buttons[i].SetActive(true);
			} else
				buttons[i].SetActive(false);
		}
	}

	public void HideButtons ()
	{
		foreach (GameObject button in buttons) {
			button.SetActive(false);
		}
	}
}
