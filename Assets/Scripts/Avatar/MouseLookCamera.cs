﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MouseLookCamera : MonoBehaviour {

	public float lookSpeed, walkSpeed;
	public Transform cameraTransform;

	private Vector2 clickStart;
	private bool dragging;
	private Transform myTransform;
	private float cameraStartY, cameraStartX;

	void Start () {
		myTransform = transform;
		dragging = false;
	}
	
	void Update () {
		Vector3 moveVector = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
		myTransform.Translate(moveVector * walkSpeed * Time.deltaTime);

		if (Input.GetMouseButtonDown(1)) {
			dragging = true;
			clickStart = Input.mousePosition;
			cameraStartX = cameraTransform.localEulerAngles.x;
			cameraStartY = myTransform.localEulerAngles.y;
		}

		if (Input.GetMouseButtonUp(1))
			dragging = false;

		if (dragging) {
			myTransform.localEulerAngles = new Vector3(0, (cameraStartY - (clickStart.x - Input.mousePosition.x) * lookSpeed));
			cameraTransform.localEulerAngles = new Vector3((cameraStartX + (clickStart.y - Input.mousePosition.y) * lookSpeed), 0, 0);
		}

		if (Input.GetKeyDown(KeyCode.Escape))
			Application.Quit();

		if (Input.GetKeyDown(KeyCode.R))
			SceneManager.LoadScene(0);
	}
}
