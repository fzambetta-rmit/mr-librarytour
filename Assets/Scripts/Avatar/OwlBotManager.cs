﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RIGLabs;
using RIGLabs.EventManager;
using Vuforia;

public class OwlBotManager : Singleton<OwlBotManager>
{

    private CharacterManager characterManager;

    public CharacterManager characterPrefab;

    public int debugPosterIndex;

    private void Awake()
    {
        if (characterManager == null)
            characterManager = GameObject.FindObjectOfType<CharacterManager>();

        if (characterManager == null)
            characterManager = GameObject.Instantiate<CharacterManager>(characterPrefab);

        characterManager.Hide();
    }

    /*private void OnEnable()
    {
        EventManager.StartListening(Events.PosterDetected, TriggerRandomPoster);
    }

    private void OnDisable()
    {
        EventManager.StopListening(Events.PosterDetected, TriggerRandomPoster);
    }

    void TriggerRandomPoster()
    {
        characterManager.ShowPoster(ContentManager.Instance.GetPosterFromOrder(Random.Range(1, 10)));
    }*/

    public void displaypostercontent(int poster)
    {
        characterManager.ShowPoster(ContentManager.Instance.GetPosterFromOrder(poster));
    }

    public void DisplayPosterContent(int poster, Transform vumarkTransform)
    {
        characterManager.ShowPoster(ContentManager.Instance.GetPosterFromVuforiaKey(poster), vumarkTransform);
    }

    public CharacterManager GetCharacterManager()
    {
        return characterManager;
    }

    [ContextMenu("Test Poster")]
    void DebugTestPoster()
    {
        //characterManager.Initialize(debugPoster);
        characterManager.ShowPoster(ContentManager.Instance.GetPosterFromOrder(debugPosterIndex));
    }
}
