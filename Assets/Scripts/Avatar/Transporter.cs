﻿///Summary
///Star-Trek style transporter visual effect
///Instantiate this whenever the owl needs to appear

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Transporter : MonoBehaviour {

	public Animator myAnimator;
	public ParticleSystem myParticles;
	public GameObject beamMesh;
	public Renderer beamRenderer;
	[Tooltip("In seconds")]
	public float fadeDuration;
	public Material owlbotMaterial;
	public Texture opacityLines1, opacityLines2, opacityLines3;

	private const float beamStart = 0.5f;   // delay from particles starting to drawing the transporter effect
	private const float fadeStart = 3.5f;

	void Start () {
		beamMesh.SetActive(false);
		StartCoroutine(DrawEffect());

		// owlbot will be a singleton so we can access it with something like this:
		// Owlbot.instance.Appear();

		owlbotMaterial.SetTexture("_OpacityLines", opacityLines3); // opacityLines3 texture is more transparent
		owlbotMaterial.SetTexture("_OpacityLines2", opacityLines3);
	}
	
	private IEnumerator DrawEffect ()
	{
		yield return new WaitForSeconds(beamStart);
		beamMesh.SetActive(true);
		myAnimator.SetTrigger("beamIn");
		yield return new WaitForSeconds(1);
		owlbotMaterial.SetTexture("_OpacityLines", opacityLines2); // owl becomes more opaque
		owlbotMaterial.SetTexture("_OpacityLines2", opacityLines2); // owl becomes more opaque
		yield return new WaitForSeconds(1);
		owlbotMaterial.SetTexture("_OpacityLines", opacityLines1); // owl reaches final opacity
		owlbotMaterial.SetTexture("_OpacityLines2", opacityLines1);
		yield return new WaitForSeconds(0.5f);
		// start fading out beam
		Color initialColor = beamRenderer.material.GetColor("_ColorTint");
		Color currentColor = initialColor;
		//Debug.Log("Current color: " + currentColor.ToString());
		float startFadeTime = Time.time;
		float endFadeTime = Time.time + fadeDuration;
		while (currentColor.b > 0) {
			//Debug.Log(currentColor);
			yield return new WaitForEndOfFrame();
			float t = Mathf.InverseLerp(startFadeTime, endFadeTime, Time.time);
			currentColor = Color.Lerp(initialColor, Color.black, t);
			beamRenderer.material.SetColor("_ColorTint", currentColor);
		}
		// fade out completed, destroy this instance
		

		Destroy(this.gameObject);
	}
}
