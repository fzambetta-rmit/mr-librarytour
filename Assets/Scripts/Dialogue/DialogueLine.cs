﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Dialogue/Line", order = 1)]
public class DialogueLine : ScriptableObject {
    //public PosterInfo Poster; // This is more just useability.
	public string TitleText = "";
	[TextArea]
	public string BodyText = "";
	public string URL = "";
	public CharacterManager.Platforms Platform = CharacterManager.Platforms.all;
	public List<CharacterManager.Gesture> gestures = new List<CharacterManager.Gesture>();

}
