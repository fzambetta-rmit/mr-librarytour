﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;

[CustomEditor(typeof(PosterInfo))]
[CanEditMultipleObjects]
public class PosterInfoEditor : Editor {
	private PosterInfo posterObject;
    private FoldOutData linesFoldout;
    private List<FoldOutData> lineDataList = new List<FoldOutData>();

    class FoldOutData
	{
		public bool Opened;
		public int Count;
		public string Name;
		public string Property;
		public System.Type Type;
		public System.Action OnFoldoutCallback;
        public GUIStyle foldoutStyle;
		//public object onCompleteParam { get; set; }

		public FoldOutData(bool opened, System.Action onFoldoutCallback, string name, string property, System.Type type)
		{
			Opened = opened;
			Count = 0;
			Name = name;
			Property = property;
			Type = type;
			OnFoldoutCallback = onFoldoutCallback;
            foldoutStyle = EditorStyles.foldout;
		}
	}

	// Use this for initialization
	void OnEnable()
    {
        posterObject = (PosterInfo)target;

        lineDataList.Clear();
        for (int i = 0; i < posterObject.Lines.Count; i++)
        {
            AddNewLineFoldOutData();
        }

        linesFoldout = new FoldOutData(true, null, "Lines", "DialogueLine", typeof(DialogueLine));
    }

    void AddNewLineFoldOutData(bool opened = false)
    {
        lineDataList.Add(new FoldOutData(opened, null, "Line" + (lineDataList.Count + 1).ToString(), "DialogueLine", typeof(DialogueLine)));
    }

    void CreateNewDialogue()
    {
        string directory = posterObject.NewDialogueDirectory;

        int count = posterObject.Lines.Count;
        // Places the new dialogue file where the other ones else use the default.
        if ( count > 0)
        {
            directory = Path.GetDirectoryName(AssetDatabase.GetAssetPath(posterObject.Lines[0]));
        }

        DialogueLine newLine = ScriptableObject.CreateInstance<DialogueLine>();
        //newLine.Poster = posterObject;
        if (count > 0)
            newLine.TitleText = posterObject.Lines[count - 1].TitleText;
        
        string newFilePath = string.Format(posterObject.NewDialogueFilename, directory, posterObject.name, posterObject.Lines.Count);
        newFilePath = AssetDatabase.GenerateUniqueAssetPath(newFilePath);
        AssetDatabase.CreateAsset(newLine, newFilePath);
        posterObject.Lines.Add(UnityEditor.AssetDatabase.LoadAssetAtPath<DialogueLine>(newFilePath));
        AddNewLineFoldOutData(true);

	}

    void AddNewDialogue()
    {
        posterObject.Lines.Add(null);
        AddNewLineFoldOutData(true);
    }

    void RemoveDialogue(DialogueLine line)
    {
        //int index = posterObject.Lines.FindIndex(line);
        posterObject.Lines.Remove(line);
        //lineDataList.RemoveAt(index);
    }

	void DeleteDialogue(DialogueLine line)
	{
        RemoveDialogue(line);
        AssetDatabase.DeleteAsset(AssetDatabase.GetAssetPath(line));
	}

    void ExpandAllLines(bool open)
    {
        for (int i = 0; i < lineDataList.Count; i++)
        {
            lineDataList[i].Opened = open;
        }
    }

    void AppendToLineFoldOutName(ref string original, string text, ref int lineCount)
    {
        if (!string.IsNullOrEmpty(text))
        {
            original += "\n" + text;
            lineCount++;
        }
    }

    void DrawLineFoldOut(DialogueLine line, FoldOutData lineData, int index)
    {
        //EditorGUILayout.Foldout(foldout.Opened, foldout.Name + " (" + foldout.Count + ")", true);
        string foldOutName = "";
        int lineCount = 1;
        if (line == null)
            foldOutName = "None";
        else 
        {
            foldOutName = line.TitleText;
            if (line.Platform != CharacterManager.Platforms.all)
                foldOutName += " ["+line.Platform.ToString()+"]";
            //if (lineData.Opened == false)
            {
                AppendToLineFoldOutName(ref foldOutName, line.BodyText, ref lineCount);
                AppendToLineFoldOutName(ref foldOutName, line.URL, ref lineCount); 
            }
        }

        float lineHeight = EditorStyles.foldout.lineHeight;
        lineData.foldoutStyle.margin = new RectOffset( 0,  0,  (int)lineHeight * 2,  (int)lineHeight * lineCount);
        lineData.Opened = EditorGUILayout.Foldout(lineData.Opened, new GUIContent(foldOutName), true, lineData.foldoutStyle);
        if (lineData.Opened)
        {
            Editor tmpEditor = Editor.CreateEditor(line);
            GUILayout.BeginVertical();
            GUILayout.BeginHorizontal();

            posterObject.Lines[index] = (DialogueLine)EditorGUILayout.ObjectField(new GUIContent(""), posterObject.Lines[index], typeof(DialogueLine), false);

            if (GUILayout.Button("Remove"))
            {
                RemoveDialogue(line);
                EditorUtility.SetDirty(target);
            }

            if (GUILayout.Button("Delete"))
            {
                DeleteDialogue(line);
                EditorUtility.SetDirty(target);
            }

            GUILayout.EndHorizontal();

            GUILayout.EndVertical();
            GUILayout.BeginVertical();
            EditorGUI.indentLevel++;
            if (line != null)
               tmpEditor.OnInspectorGUI();
            EditorGUI.indentLevel--;

            GUILayout.EndVertical();
        }
    }

	void DrawFoldOut(FoldOutData foldout)
	{
		foldout.Opened = EditorGUILayout.Foldout(foldout.Opened, foldout.Name + " (" + foldout.Count + ")", true);
        EditorGUI.indentLevel++;


		if (foldout.Opened)
		{

            EditorGUI.BeginDisabledGroup(posterObject.Lines.Count <= 0);
            if (GUILayout.Button("Expand All Lines"))
            {
                ExpandAllLines(true);
                linesFoldout.Opened = true;
            }
            if (GUILayout.Button("Collapse All Lines"))
            {
                ExpandAllLines(false);
                linesFoldout.Opened = true;
            }
            EditorGUI.EndDisabledGroup();

			if (foldout.OnFoldoutCallback != null)
			{
				foldout.OnFoldoutCallback();
			}

            EditorGUI.indentLevel++;
            // Drawing each Line
            for (int i = 0; i < foldout.Count; i++)
            {
                DrawLineFoldOut(posterObject.Lines[i], lineDataList[i], i);
			}
            EditorGUI.indentLevel--;

            // Add Button
			if (GUILayout.Button("Add Line"))
			{
                AddNewDialogue();
				EditorUtility.SetDirty(target);
			}

            if (GUILayout.Button("Create NEW Line"))
            {
                CreateNewDialogue();
                EditorUtility.SetDirty(target);
            }

            if (GUILayout.Button("Load Lines"))
            {
                if (posterObject.LoadLines())
                    EditorUtility.SetDirty(target);
            }

			
        }
        EditorGUI.indentLevel--;
	}

	public override void OnInspectorGUI()
	{
		serializedObject.Update();
        posterObject = (PosterInfo)target;

		// Show default inspector property editor
		EditorGUI.BeginChangeCheck();
        EditorGUILayout.LabelField(posterObject.name  + " Poster", EditorStyles.boldLabel);
		DrawDefaultInspector();
        linesFoldout.Count = posterObject.Lines.Count;


        DrawFoldOut(linesFoldout);
		
		if (EditorGUI.EndChangeCheck())
		{
			serializedObject.ApplyModifiedProperties();
			EditorUtility.SetDirty(target);
		}
	}
}
