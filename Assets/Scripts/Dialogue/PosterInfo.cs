﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Dialogue/PosterInfo", order = 1)]
public class PosterInfo : ScriptableObject
{
	public int order;
    public int vuforiaKey;
    public PosterInfo NextPoster;
    [HideInInspector]
	public List<DialogueLine> Lines;

    [HideInInspector]
    public OwlBotPosterLocator OwlBotLocation;

#if UNITY_EDITOR

	/// <summary>
	/// The new dialogue filename.
	/// "{0}/{1}_{2}.asset" 
	/// Directory + Name_x.asset
	/// </summary>
    [HideInInspector]
	public string NewDialogueFilename = "{0}/{1}_{2}.asset"; // Directory + Name_x.asset
	[HideInInspector]
    public string NewDialogueDirectory = "Assets/ScriptableObjects/Lines";

    [ContextMenu("Load Lines")]
	public bool LoadLines()
	{
        //List<DialogueLine> searchLines = new List<DialogueLine>();
        //PostersAssets.Clear();
        bool changed = false;
        string[] guids = UnityEditor.AssetDatabase.FindAssets(string.Format("{0} t:{1} ", name, typeof(DialogueLine)));
		for (int i = 0; i < guids.Length; i++)
		{
			string assetPath = UnityEditor.AssetDatabase.GUIDToAssetPath(guids[i]);
			DialogueLine asset = UnityEditor.AssetDatabase.LoadAssetAtPath<DialogueLine>(assetPath);
			if (asset != null)
			{
                if (Lines.Contains(asset) == false)
                {
					Lines.Add(asset);
                    changed = true;
                } 
			}
		}

        if (changed)
            Lines.Sort((x, y) => x.name.CompareTo(y.name));
        return changed;
	}
#endif
}
