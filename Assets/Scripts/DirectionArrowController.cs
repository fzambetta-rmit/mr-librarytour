﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RIGLabs.EventManager;
using DG.Tweening;

public class DirectionArrowController : MonoBehaviour {
    public GameObject arrowModel;
    public bool isVisible = true;
    
    public Transform targetTransform;

    private Transform myTransform;
    private Transform cameraTransform;
    private Camera mainCamera;
    private Transform owlBotTransform;
    private Transform posterLocationTransform;

    [Tooltip("Allowable percentage inside the holographic frame to continue to show a directional indicator.")]
    [Range(-0.3f, 0.3f)]
    public float VisibilitySafeFactor = 0.1f;

    // Use this for initialization
    void Start () {
        myTransform = transform;
        cameraTransform = Camera.main.transform;
        mainCamera = Camera.main;
        owlBotTransform = OwlBotManager.Instance.GetCharacterManager().transporterLocation;
    }

    private void OnEnable()
    {
        EventManager.StartListening(Events.NewPosterTarget, OnNewPosterTarget);
    }

    private void OnDestroy()
    {
        EventManager.StopListening(Events.NewPosterTarget, OnNewPosterTarget);
    }

    void OnNewPosterTarget ()
    {
        posterLocationTransform = EventManager.GetTriggerData<PosterInfo>().OwlBotLocation.transform;
    }

    // Update is called once per frame
    void Update () {
        if (CharacterManager.owlActive)
        {
            // Check if we are looking at owlbot or not.
            if (OwlBotIsVisible())
            {
                Hide();
            }
            else
            {
                targetTransform = owlBotTransform;
                Show();
            }
        }
        else if (posterLocationTransform != null)
        {
            targetTransform = posterLocationTransform;
            Show();
        }
        else
        {
            Hide();
        }

        if (isVisible)
        {
            myTransform.forward = targetTransform.position - myTransform.position;
            /*if (arrowModel != null)
                arrowModel.transform.Rotate(Vector3.forward, 1f);*/
        }
	}

    bool OwlBotIsVisible ()
    {
        // This will return true if the target's mesh is within the Main Camera's view frustums.
        if (CharacterManager.owlActive == false)
            return false;
        Vector3 position = owlBotTransform.position;
        Vector3 targetViewportPosition = mainCamera.WorldToViewportPoint(position);
        return (targetViewportPosition.x > VisibilitySafeFactor && targetViewportPosition.x < 1 - VisibilitySafeFactor &&
                targetViewportPosition.y > VisibilitySafeFactor && targetViewportPosition.y < 1 - VisibilitySafeFactor &&
                targetViewportPosition.z > 0);
    }

    public void Hide ()
    {
        if (arrowModel != null)
        {
            isVisible = false;
            arrowModel.SetActive(false);
        }
    }

    public void Show()
    {
        if (arrowModel != null)
        {
            isVisible = true;
            arrowModel.SetActive(true);
        }
    }

}
