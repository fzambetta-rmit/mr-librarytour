﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.XR.WSA.Persistence;
using UnityEngine.XR.WSA;
using UnityEngine;

public class AnchorManager : MonoBehaviour {

    public GameObject rootGameObject;
    private UnityEngine.XR.WSA.WorldAnchor gameRootAnchor;

    // Use this for initialization
    void Start () {
        gameRootAnchor = rootGameObject.GetComponent<UnityEngine.XR.WSA.WorldAnchor>();
        if (gameRootAnchor == null)
        {
            gameRootAnchor = rootGameObject.AddComponent<UnityEngine.XR.WSA.WorldAnchor>();
        }
    }
}