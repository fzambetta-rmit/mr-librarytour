﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LocationTrigger : MonoBehaviour
{
    private TimeandLocation tm;
    private OwlBotManager ob;

    public int triggerIndex;
    public Transform posterLocation;
    public Transform owlbotTransform;
    public GameObject tutorialPanel;
    private ToggleLockAnchor tla;

    private void Start()
    {
        GetComponent<CapsuleCollider>().enabled = false;
        GetComponent<Renderer>().enabled = true;

        // turn off renderer
        // Destroy(GetComponent<Renderer>());
        ob = GameObject.Find("OwlBot Manager Script").GetComponent<OwlBotManager>();
        tm = GameObject.Find("Time + Location").GetComponent<TimeandLocation>();
        tla = GameObject.Find("Managers").GetComponent<ToggleLockAnchor>();

        if (tla.areLocked[(int)ID.One] == true && tla.areLocked[(int)ID.Two] == true && tla.areLocked[(int)ID.Three] == true && tla.areLocked[(int)ID.Four] == true)
        {
            GetComponent<CapsuleCollider>().enabled = true;
            GetComponent<Renderer>().enabled = false;
        }

        if (triggerIndex == 1 && tla.areLocked[(int)ID.One] == true && tla.areLocked[(int)ID.Two] == true && tla.areLocked[(int)ID.Three] == true && tla.areLocked[(int)ID.Four] == true)
        {
            owlbotTransform.position = posterLocation.position;
            owlbotTransform.rotation = posterLocation.rotation;

            ob.displaypostercontent(triggerIndex);
            tm.incrementNumber = (triggerIndex * 6).ToString();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        owlbotTransform.position = posterLocation.position;
        owlbotTransform.rotation = posterLocation.rotation;

        ob.displaypostercontent(triggerIndex);
        tm.incrementNumber = (triggerIndex * 6).ToString();

        // disable collision with this trigger 
        GetComponent<CapsuleCollider>().enabled = false;
        GetComponent<Renderer>().enabled = false;

        tm.displayowlbot();
    }

    private void OnDrawGizmos()
    {
        // visualize the poster location
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(posterLocation.position, 0.15f);
        Gizmos.DrawLine(posterLocation.position, posterLocation.position - posterLocation.forward * 0.5f);
        Gizmos.DrawSphere(posterLocation.position - posterLocation.forward * 0.5f, 0.03f);
        Gizmos.color = Color.black;
        Gizmos.DrawLine(posterLocation.position, transform.position);
    }

    public void onCallReset()
    {
        GetComponent<CapsuleCollider>().enabled = true;
        GetComponent<Renderer>().enabled = false;
    }
    public void OndebugCallOn()
    {
        GetComponent<Renderer>().enabled = true;
        GetComponent<CapsuleCollider>().enabled = false;
    }
    public void OndebugCallOff()
    {
        GetComponent<Renderer>().enabled = false;
        GetComponent<CapsuleCollider>().enabled = true;
    }

}
