﻿using HoloToolkit.Unity.InputModule;
using UnityEngine;
using UnityEngine.UI;

public enum ID { One=0, Two=1, Three=2, Four=3};

public class ToggleLockAnchor : MonoBehaviour
{
    public Text lockMessage;
    public Text unLockMessage;
    private float time = 5;

    [SerializeField]
    public bool[] areLocked;
    public GameObject[] handles;
    public GameObject[] sprites;

    
    

    private void Awake()
    {
#if !UNITY_EDITOR
       areLocked[(int)ID.One] = PlayerPrefs.GetInt("isLockedOne") == 1 ? true : false;
       areLocked[(int)ID.Two] = PlayerPrefs.GetInt("isLockedTwo") == 1 ? true : false;
       areLocked[(int)ID.Three] = PlayerPrefs.GetInt("isLockedThree") == 1 ? true : false;
       areLocked[(int)ID.Four] = PlayerPrefs.GetInt("isLockedFour") == 1 ? true : false;
#endif
    }

    private void Start()
    {
        lockMessage.enabled = false;
        unLockMessage.enabled = false;

        for (int i=0; i<4; i++)
        {
            if (areLocked[i])
            {
                handles[i].SetActive(false);
                sprites[i].SetActive(false);
            }
        }
    }

    void StartCoundownTimer()
    {
        time = 5;
        InvokeRepeating("UpdateTimer", 0.0f, 0.01667f);
    }

    void UpdateTimer()
    {
        time -= Time.deltaTime;

        if (time < 0)
        {
            lockMessage.enabled = false;
            unLockMessage.enabled = false;
        }
    }


    public void VoiceLockAnchor(ID anchorNum,bool areWeLocking)
    {
        areLocked[(int)anchorNum] = areWeLocking;
        PlayerPrefs.SetInt("isLocked"+anchorNum.ToString(), areLocked[(int)anchorNum] ? 1 : 0);

        if (areWeLocking)
            lockMessage.enabled = true;
        else
            unLockMessage.enabled = true;

        time = 5;
        StartCoundownTimer();
        handles[(int)anchorNum].SetActive(!areWeLocking);
        sprites[(int)anchorNum].SetActive(!areWeLocking);
    }
}
