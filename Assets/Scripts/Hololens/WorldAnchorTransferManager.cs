﻿using HoloToolkit.Unity.InputModule;
using RIGLabs;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.XR.WSA;
using UnityEngine.XR.WSA.Sharing;

/// <summary>
/// Author: Ranjit Singh
/// Date created: 07/04/18
/// Purpose: Manages exporting and importing of world anchor data from remote server.
/// </summary>
public class WorldAnchorTransferManager : Singleton<WorldAnchorTransferManager>
{
	public string apiUrl;

	static WorldAnchorTransferManager _instance;

	byte[] exportBytes;
	
	/// <summary>
	/// Starts the export process and then uploads to remote server once the data is ready.
	/// </summary>
	/// <param name="worldAnchors">Achors to export and upload</param>
	public void Upload(WorldAnchor[] worldAnchors)
	{
		if(worldAnchors == null || worldAnchors.Length == 0)
		{
			Debug.LogError("No anchors available to export.");
		}
		
		exportBytes = new byte[0];
		
		WorldAnchorTransferBatch transferBatch = new WorldAnchorTransferBatch();

		foreach (var anchor in worldAnchors)
		{
			transferBatch.AddWorldAnchor(anchor.gameObject.name.ToString(), anchor);
		}

		WorldAnchorTransferBatch.ExportAsync(transferBatch, OnExportDataAvailable, OnExportComplete);
	}

	public void Download()
	{
		throw new NotImplementedException();
	}
	
	IEnumerator UploadData(byte[] data, Action<bool> callback = null)
	{
		WWWForm form = new WWWForm();
		form.AddBinaryData("file", data, "WorldAnchor.dat");

		var request = UnityWebRequest.Post(apiUrl, form);
		yield return request.SendWebRequest();

		// Report back request success or failure
		if (callback != null)
		{
			callback(!request.isHttpError && !request.isNetworkError);
		}
	}

	IEnumerator DownloadData(Action<bool, byte[]> callback = null)
	{
		var request = UnityWebRequest.Get(apiUrl);
		yield return request.SendWebRequest();

		// Report back request success or failure
		if (callback != null)
		{
			callback(!request.isHttpError && !request.isNetworkError,
					 request.downloadHandler.data);
		}
	}

	void OnExportDataAvailable(byte[] data)
	{
		exportBytes = exportBytes.Concat(data).ToArray();
	}

	void OnExportComplete(SerializationCompletionReason completionReason)
	{
		if (completionReason != SerializationCompletionReason.Succeeded)
		{
			Debug.LogError("Failed serialization: " + completionReason.ToString());
		}
		else
		{
			try
			{
				StartCoroutine(UploadData(exportBytes, OnUploadComplete));
			}
			catch (Exception ex)
			{
				Debug.LogError("Failed serialization: " + ex.ToString());
			}
		}
	}

	void OnUploadComplete(bool success)
	{
		Debug.Log("Upload " + (success ? "successful" : "failed"));
	}
}
