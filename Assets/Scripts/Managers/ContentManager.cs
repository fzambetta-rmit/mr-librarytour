﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RIGLabs;

public class ContentManager : Singleton<ContentManager>
{
    [SerializeField]
    private List<PosterInfo> PostersAssets;

    public PosterInfo GetPosterFromOrder(int index)
    {
        return PostersAssets.Find((obj) => obj.order == index);
    }

    public PosterInfo GetPosterFromVuforiaKey(int key)
    {
        return PostersAssets.Find((obj) => obj.vuforiaKey == key);
    }



#if UNITY_EDITOR
    [ContextMenu("Load Posters")]
    public void LoadPosterAssets()
    {
        PostersAssets.Clear();

        string[] guids = UnityEditor.AssetDatabase.FindAssets(string.Format("t:{0}", typeof(PosterInfo)));
        for (int i = 0; i < guids.Length; i++)
        {
            string assetPath = UnityEditor.AssetDatabase.GUIDToAssetPath(guids[i]);
            PosterInfo asset = UnityEditor.AssetDatabase.LoadAssetAtPath<PosterInfo>(assetPath);
            if (asset != null)
            {
                /*if (asset.AssetPath != assetPath)
                    asset.SetAssetPath(assetPath); // This should be redudant but JUST in case.*/
                PostersAssets.Add(asset);
            }
        }
        PostersAssets.Sort((x, y) => x.order.CompareTo(y.order));
    }
#endif
}
