﻿// Simple rotator script - used on the solar system model to orbit the planets

using UnityEngine;

public class ConstantRotation : MonoBehaviour {

	public float speed;

	private Transform myTransform;

	private void Start()
	{
		myTransform = GetComponent<Transform>();
	}

	void Update () {
		myTransform.Rotate(0, 0, speed * Time.deltaTime);
	}
}
