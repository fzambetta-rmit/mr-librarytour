﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RIGLabs.EventManager
{
	public enum Events
	{
		MainMenu,
		PosterDetected,
        DialogueFinished,
        OneLock,
        OneUnLock,
        TwoLock,
        TwoUnLock,
        ThreeLock,
        ThreeUnLock,
        FourLock,
        FourUnLock,
        OwlBotReset,
        NewPosterTarget,
        DebugOn,
        DebugOff,
        ResetOne,
        ResetTwo,
        ResetThree,
        ResetFour,
        togglemesh,
        //--------
        TOTAL
	}
}