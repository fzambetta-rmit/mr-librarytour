﻿/// Summary
/// Hides the renderer on an object - useful for triggers that need to be invisible at runtime.

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HideRenderer : MonoBehaviour {

	void Start () {
		Destroy(GetComponent<Renderer>());
	}

}
