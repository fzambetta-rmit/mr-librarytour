﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RIGLabs;
using RIGLabs.EventManager;
using HoloToolkit.Unity.InputModule;
using HoloToolkit.Unity;

public class VirtualEnvironmentModel : MonoBehaviour
{
    public ToggleLockAnchor togglelockanchor;
    public TimeandLocation timeandlocation;
    public CharacterManager cm;
    public TapAnchor ta;
	public GameObject beachScene; // need to turn it on and off when "Debug On" is spoken

    public LocationTrigger[] Triggers;

    public Transform[] Anchors;

    void Awake()
    {
        EventManager.StartListening(Events.OneLock, OneLock);
        EventManager.StartListening(Events.OneUnLock, OneUnLock);

        EventManager.StartListening(Events.TwoLock, TwoLock);
        EventManager.StartListening(Events.TwoUnLock, TwoUnLock);

        EventManager.StartListening(Events.ThreeLock, ThreeLock);
        EventManager.StartListening(Events.ThreeUnLock, ThreeUnLock);

        EventManager.StartListening(Events.FourLock, FourLock);
        EventManager.StartListening(Events.FourUnLock, FourUnLock);

        EventManager.StartListening(Events.OwlBotReset, OwlBotReset);

        EventManager.StartListening(Events.DebugOn, DebugOn);
        EventManager.StartListening(Events.DebugOff, DebugOff);

        EventManager.StartListening(Events.ResetOne, ResetOne);
        EventManager.StartListening(Events.ResetTwo, ResetTwo);
        EventManager.StartListening(Events.ResetThree, ResetThree);
        EventManager.StartListening(Events.ResetFour, ResetFour);

        EventManager.StartListening(Events.togglemesh, Togglemesh);
    }
    
    private void Update()
    {
        if (Input.GetKeyDown("space"))
        {
            OwlBotReset();
        }
        if (Input.GetKeyDown("up"))
        {
            DebugOn();
        }
        if (Input.GetKeyDown("down"))
        {
            DebugOff();
        }
        if (Input.GetKeyDown("left"))
        {
            ResetTwo();
        }
        if (Input.GetKeyDown("right"))
        {
            Togglemesh();
        }
    }

    void OneLock()
    {
        togglelockanchor.VoiceLockAnchor(ID.One,true);
    }
    void OneUnLock()
    {
        togglelockanchor.VoiceLockAnchor(ID.One, false);
    }
    void TwoLock()
    {
        togglelockanchor.VoiceLockAnchor(ID.Two, true);
    }
    void TwoUnLock()
    {
        togglelockanchor.VoiceLockAnchor(ID.Two, false);
    }
    void ThreeLock()
    {
        togglelockanchor.VoiceLockAnchor(ID.Three, true);
    }
    void ThreeUnLock()
    {
        togglelockanchor.VoiceLockAnchor(ID.Three, false);
    }
    void FourLock()
    {
        togglelockanchor.VoiceLockAnchor(ID.Four, true);
    }
    void FourUnLock()
    {
        togglelockanchor.VoiceLockAnchor(ID.Four, false);
    }

    void OwlBotReset()
    {
        timeandlocation.OnCallReset();
        cm.OnResetCall();
        foreach (LocationTrigger obj in Triggers)
        {
            obj.onCallReset();
        }
    }

    void DebugOn()
    {
        foreach (LocationTrigger obj in Triggers)
        {
            obj.OndebugCallOn();
        }
        timeandlocation.OnDebugCallOn();
        timeandlocation.returnAnimation.SetActive(true);
		beachScene.SetActive(true);
    }
    void DebugOff()
    {
        foreach (LocationTrigger obj in Triggers)
        {
            obj.OndebugCallOff();
        }
        timeandlocation.OnDebugCallOff();
        timeandlocation.returnAnimation.SetActive(false);
		beachScene.SetActive(false);
    }

    private void Reset(ID anchorNum)
    {
        if (WorldAnchorManager.Instance != null)
            WorldAnchorManager.Instance.RemoveAnchor(GameObject.Find("LibTapAnchor"+((int)anchorNum+1)).ToString());

        Anchors[(int)anchorNum].transform.position = Camera.main.transform.position + Camera.main.transform.forward * 2;

        var fwd = Camera.main.transform.forward;
        fwd.y = 0.0f;
        Anchors[(int)anchorNum].transform.rotation = Quaternion.LookRotation(fwd);
    }

    void ResetOne()
    {
        Reset(ID.One);
    }

    void ResetTwo()
    {
        Reset(ID.Two);
    }

    void ResetThree()
    {
        Reset(ID.Three);
    }

    void ResetFour()
    {
        Reset(ID.Four);
    }

    void Togglemesh()
    {
        ta.VoiceCommandToggleSpatialMesh();
    }
}
