using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Windows.Speech;
using RIGLabs;
using RIGLabs.EventManager;

public class SpeechManager : Singleton<SpeechManager>
{
    KeywordRecognizer keywordRecognizer = null;
    Dictionary<string, System.Action> keywords = new Dictionary<string, System.Action>();

    void Start()
    {
        keywords.Add("Lock One", () => { TriggerEvent(Events.OneLock); });
        keywords.Add("UnLock One", () => { TriggerEvent(Events.OneUnLock); });

        keywords.Add("Lock Two", () => { TriggerEvent(Events.TwoLock); });
        keywords.Add("UnLock Two", () => { TriggerEvent(Events.TwoUnLock); });

        keywords.Add("Lock Three", () => { TriggerEvent(Events.ThreeLock); });
        keywords.Add("UnLock Three", () => { TriggerEvent(Events.ThreeUnLock); });

        keywords.Add("Lock Four", () => { TriggerEvent(Events.FourLock); });
        keywords.Add("UnLock Four", () => { TriggerEvent(Events.FourUnLock); });

        keywords.Add("Begin Tour", () => { TriggerEvent(Events.OwlBotReset); });

        keywords.Add("Debug on", () => { TriggerEvent(Events.DebugOn); });
        keywords.Add("Debug off", () => { TriggerEvent(Events.DebugOff); });

        keywords.Add("Reset One", () => { TriggerEvent(Events.ResetOne); });
        keywords.Add("Reset Two", () => { TriggerEvent(Events.ResetTwo); });
        keywords.Add("Reset Three", () => { TriggerEvent(Events.ResetThree); });
        keywords.Add("Reset Four", () => { TriggerEvent(Events.ResetFour); });

        keywords.Add("Toggle Mesh", () => { TriggerEvent(Events.togglemesh); });       

    // Tell the KeywordRecognizer about our keywords.
    keywordRecognizer = new KeywordRecognizer(keywords.Keys.ToArray());

        // Register a callback for the KeywordRecognizer and start recognizing!
        keywordRecognizer.OnPhraseRecognized += KeywordRecognizer_OnPhraseRecognized;
        keywordRecognizer.Start();
    }

    private void TriggerEvent(Events eventNum)
    {
        EventManager.TriggerEvent(eventNum);
    }

    private void KeywordRecognizer_OnPhraseRecognized(PhraseRecognizedEventArgs args)
    {
        Debug.Log(args.text);
        System.Action keywordAction;
        if (keywords.TryGetValue(args.text, out keywordAction))
        {
            keywordAction.Invoke();
        }
    }
}