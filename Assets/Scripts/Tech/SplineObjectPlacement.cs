﻿/// Summary
/// Creates a bezier spline from 3 control nodes
/// Can spawn objects along the spline

using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(LineRenderer))]
public class SplineObjectPlacement : MonoBehaviour {

	[Header("Curve settings")]
	public Transform[] nodes;
	public LineRenderer[] lines; // line 0 is the spline, lines 1 and 2 are the control handles
	[Tooltip("Number of steps on the spline")]
	public int resolution;
	[Tooltip("Keep this off unless you're editing the spline and need to see changes in real-time")]
	public bool updateSpline;

	[Header("Placement settings")]

	public GameObject spawnObject;
	public float spacing, objectScale, placementSteps; // placement steps is the number of checks we do along the spline - higher = more accurate object spawning
	public Transform spawnedObjectsParent;
			
	private void OnDrawGizmos()
	{
		// draw the handles
		foreach (Transform node in nodes) {
			Gizmos.DrawWireSphere(node.position, 0.1f);
		}

		// draw the spline
		if (updateSpline) {
			lines[0].positionCount = resolution + 1;

			float stepSize = 1f / resolution;
			for (int i = 0; i < lines[0].positionCount; i++) {
				lines[0].SetPosition(i, GetPointCubic(nodes[0].position, nodes[1].position, nodes[2].position, nodes[3].position, stepSize * i));
			}
		}

		// draw lines from end points to control points
		lines[1].SetPosition(0, nodes[1].position);
		lines[1].SetPosition(1, nodes[0].position);
		lines[2].SetPosition(0, nodes[2].position);
		lines[2].SetPosition(1, nodes[3].position);
	}

	
	// Quadratic or 3 point bezier curve - switching to cubic for the extra control handle
	/*
	private Vector3 GetPoint(Vector3 p0, Vector3 p1, Vector3 p2, float t)
	{
		// lerp method:
		//return Vector3.Lerp(Vector3.Lerp(p0, p1, t), Vector3.Lerp(p1, p2, t), t);

		t = Mathf.Clamp01(t);
		float oneMinusT = 1f - t;
		return
			oneMinusT * oneMinusT * p0 +
			2f * oneMinusT * t * p1 +
			t * t * p2;
	}
	*/

	private Vector3 GetPointCubic(Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3, float t)
	{
		float oneMinusT = 1f - t;
		return
			Mathf.Pow(oneMinusT, 3f) * p0 +
			3f * oneMinusT * oneMinusT * t * p1 +
			3f * oneMinusT * t * t * p2 +
			t * t * t * p3;
	}

	public void PlaceObjects ()
	{
		ClearObjects();
		Vector3 lastPlacement = nodes[0].position;
		float stepSize = 1f / placementSteps;

		// Always spawn the first object
		GameObject firstObject = Instantiate(spawnObject, nodes[0].position, Quaternion.identity, spawnedObjectsParent);
		firstObject.transform.localScale = Vector3.one * objectScale;

		// Now move along the spline until we are far enough from the last position, then spawn a new object
		for (int i = 1; i <= placementSteps; i++) {
			Vector3 spawnPoint = GetPointCubic(nodes[0].position, nodes[1].position, nodes[2].position, nodes[3].position, stepSize * i);

			if (Vector3.Distance(spawnPoint, lastPlacement) > spacing) {
				GameObject newObject = Instantiate(spawnObject, spawnPoint, Quaternion.identity, spawnedObjectsParent);
				newObject.transform.localScale = Vector3.one * objectScale;
				lastPlacement = spawnPoint;
			}
		}
	}

	public void ClearObjects()
	{
		while (spawnedObjectsParent.childCount > 0) {
			DestroyImmediate(spawnedObjectsParent.GetChild(0).gameObject);
		}
	}

	private void Start()
	{
		// remove editor components and leave the placed objects
		spawnedObjectsParent.SetParent(transform.parent.transform.parent);
		Destroy(this.gameObject);
	}
}

