﻿///Summary
///UV scrolling and oscillating - used on the beach waves

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UVScroller : MonoBehaviour {

	public Vector2 scrollRate;
	[Tooltip("Oscillate on a sine wave instead of scrolling")]
	public bool oscillateU, oscillateV;
	public Vector2 oscillationRange;

	private Renderer myRenderer;
	private Vector2 offset = Vector2.zero;

	void Start () {
		myRenderer = GetComponent<Renderer>();	
	}
	
	void Update () {
		if (!oscillateU)
			offset.x = Time.time * scrollRate.x;
		else
			offset.x = Mathf.Sin(Time.time * scrollRate.x) * oscillationRange.x;

		if (!oscillateV)
			offset.y = Time.time * scrollRate.y;
		else
			offset.y = Mathf.Sin(Time.time * scrollRate.y) * oscillationRange.y;

		myRenderer.material.SetTextureOffset("_MainTex", offset);
	}
}
