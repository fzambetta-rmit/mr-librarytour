﻿using HoloToolkit.Unity.InputModule;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimeandLocation : MonoBehaviour
{
    public string minutes;
    public string seconds;
    public string incrementNumber = "0";

    public CharacterManager charactermanager;
    public ToggleLockAnchor togglelockanchor;

    public Text timerText;
    public Text Location;
    public Text timeremainingtext;
    public Text nextlocationText;
    public Text TimesUp;
    public Text PosterNumber;
    public Text YourAtPoster;

    public GameObject[] tapanchors;
    public GameObject[] trails;
    public GameObject backgroundPanel;
    public GameObject returnAnimation;

    public Animator returnbookAnimation;

    public int numberposters = 0;

    bool timeEnded = false;

    private float time = 1800;

    public PosterInfo IntroductionPoster;
    public PosterInfo SDLCPoster;
    public PosterInfo ComputersPoster;
    public PosterInfo BookReturnPoster;
    public PosterInfo BooksPoster;
    public PosterInfo SilentStudyPoster;
    public PosterInfo ReserveCollectionPoster;
    public PosterInfo HoldPoster;
    public PosterInfo GroupWork_StarwarsPoster;
    public PosterInfo GroupWork_BalconyPoster;
    public PosterInfo BookARoomPoster;
    public List<PosterInfo> seenPosters = new List<PosterInfo>();

    void Start()
    {
        returnAnimation.SetActive(false);
        PosterNumber.text = numberposters.ToString() + "/11"; ;
        PosterNumber.enabled = true;
        YourAtPoster.enabled = true;
        timerText.enabled = true;
        Location.enabled = true;
        timeremainingtext.enabled = true;
        nextlocationText.enabled = true;
        TimesUp.enabled = false;
        backgroundPanel.SetActive(false);

        foreach (GameObject trail in trails)
        {
            trail.SetActive(true);
        }

        if (togglelockanchor.areLocked[(int)ID.One] == true && togglelockanchor.areLocked[(int)ID.Two] == true && togglelockanchor.areLocked[(int)ID.Three] == true && togglelockanchor.areLocked[(int)ID.Four] == true)
        {
            foreach (GameObject trail in trails)
            {
                trail.SetActive(false);
            }
        }
    }

    public void StartCoundownTimer()
    {
        if (timerText != null)
        {
            time = 1800;
            InvokeRepeating("UpdateTimer", 0.0f, 0.01667f);
        }
    }

    void UpdateTimer()
    {
        if (timerText != null)
        {
            time -= Time.deltaTime;
            minutes = Mathf.Floor(time / 60).ToString("00");
            seconds = (time % 60).ToString("00");
            timerText.text = minutes + ":" + seconds;
        }
    }

    public void owlbotactive()
    {
        if (timeEnded == false)
        {
            timerText.enabled = false;
            Location.enabled = false;
            timeremainingtext.enabled = false;
            nextlocationText.enabled = false;
            PosterNumber.enabled = false;
            YourAtPoster.enabled = false;
            backgroundPanel.SetActive(false);
        }
    }

    public void owlbotnotactive()
    {
        if (timeEnded == false)
        {
            timerText.enabled = true;
            Location.enabled = true;
            timeremainingtext.enabled = true;
            nextlocationText.enabled = true;
            PosterNumber.enabled = true;
            YourAtPoster.enabled = true;
            backgroundPanel.SetActive(true);
        }
    }

    void Update()
    {
        if (minutes.Equals("00") && seconds.Equals("00"))
        {
            timeEnded = true;
            TimeUp();
        }
    }

    public void TimeUp()
    {
        TimesUp.enabled = true;
        timerText.enabled = false;
        Location.enabled = false;
        timeremainingtext.enabled = false;
        nextlocationText.enabled = false;
        PosterNumber.enabled = false;
        YourAtPoster.enabled = false;
    }

    public void displayReturnAnimation(PosterInfo currentPoster)
    {
        if (currentPoster == BookReturnPoster)
        {
            returnAnimation.SetActive(true);
            returnbookAnimation.SetTrigger(1);
        }
        else
        {
            returnAnimation.SetActive(false);
        }
    }

    public void displayowlbot()
    {
        switch (incrementNumber)
        {
            case "0":
                Location.text = "Welcome to the LibGo app";
                break;
            case "6":
                Location.text = "Follow the trail to your right across the bridge to get to the Study and Learning Centre";
                numberposters = IntroductionPoster.order;
                seenPosters.Add(IntroductionPoster);
                break;
            case "12":
                Location.text = "Follow the trail back the way you came to get to the computers in the level 5 entrance";
                trails[0].SetActive(false);
                numberposters = SDLCPoster.order;
                seenPosters.Add(SDLCPoster);
                break;
            case "18":
                Location.text = "Follow the trail to your right to get to the book return chute";
                trails[1].SetActive(false);
                numberposters = ComputersPoster.order;
                seenPosters.Add(ComputersPoster);
                break;
            case "24":
                Location.text = "Now go through the door to your right to get to the books";
                trails[2].SetActive(false);
                numberposters = BookReturnPoster.order;
                seenPosters.Add(BookReturnPoster);
                break;
            case "30":
                Location.text = "The tour continues on Level 6. You can take these stairs or the lift over here to get to the next location";
                trails[3].SetActive(false);
                numberposters = BooksPoster.order;
                seenPosters.Add(BooksPoster);
                break;
            case "36":
                if (seenPosters.Contains(ReserveCollectionPoster))
                {
                    Location.text = "Walk over here to see the holds shelf";
                    trails[4].SetActive(false);
                    trails[5].SetActive(false);
                }
                else
                {
                    Location.text = "Follow the trail to your left to get to the reserve collection";
                    trails[4].SetActive(false);
                    trails[5].SetActive(false);
                }
                numberposters = SilentStudyPoster.order;
                seenPosters.Add(SilentStudyPoster);
                break;
            case "42":
                if (seenPosters.Contains(SilentStudyPoster))
                {
                    Location.text = "Follow the trail to your right to get to the holds shelf";
                    trails[6].SetActive(false);
                    trails[7].SetActive(false);
                }
                else
                {
                    Location.text = "Follow the trail to your left to get to the silent study area";
                    trails[6].SetActive(false);
                    trails[7].SetActive(false);
                }
                numberposters = ReserveCollectionPoster.order;
                seenPosters.Add(ReserveCollectionPoster);
                break;
            case "48":
                Location.text = "Now walk back this way past the level 6 library service desk to see a group work area";
                trails[8].SetActive(false);
                trails[6].SetActive(false);
                numberposters = HoldPoster.order;
                seenPosters.Add(HoldPoster);
                break;
            case "54":
                Location.text = "Now follow the trail back this way to another great group work area";
                trails[9].SetActive(false);
                trails[10].SetActive(false);
                numberposters = GroupWork_StarwarsPoster.order;
                seenPosters.Add(GroupWork_StarwarsPoster);
                break;
            case "60":
                Location.text = "Follow the trail back this way to the last location on the tour";
                trails[11].SetActive(false);
                trails[12].SetActive(false);
                trails[13].SetActive(false);
                numberposters = GroupWork_BalconyPoster.order;
                seenPosters.Add(GroupWork_BalconyPoster);
                break;
            case "66":
                Location.text = "Take this escalator down to the library's level 5 service desk";
                trails[14].SetActive(false);
                trails[15].SetActive(false);
                numberposters = BookARoomPoster.order;
                seenPosters.Add(BookARoomPoster);
                break;
        }
        PosterNumber.text = numberposters.ToString() + "/11";
    }

    public void OnCallReset()
    {
        CancelInvoke("UpdateTimer");
        foreach (GameObject anchor in tapanchors)
        {
            anchor.GetComponent<TapAnchor>().ResetCall();
        }
        foreach (GameObject trail in trails)
        {
            trail.SetActive(false);
        }
        foreach (GameObject anchor in tapanchors)
        {
            anchor.GetComponent<TapAnchor>().VoiceCommandToggleSpatialMesh();
        }
        incrementNumber = "0";
        numberposters = 0;
        timeEnded = false;
        time = 1800;
        seenPosters.Clear();
        PosterNumber.enabled = true;
        YourAtPoster.enabled = true;
        timerText.enabled = true;
        Location.enabled = true;
        timeremainingtext.enabled = true;
        nextlocationText.enabled = true;
        TimesUp.enabled = false;
    }

    public void OnDebugCallOff() 
    {
            foreach (GameObject trail in trails)
            {
                trail.SetActive(false);
            }
    }
    public void OnDebugCallOn()
    {
            foreach (GameObject trail in trails)
            {
                trail.SetActive(true);
            }
    }
}
