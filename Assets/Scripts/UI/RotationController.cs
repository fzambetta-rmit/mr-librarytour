﻿/// Summary
/// GUI for rotating something by a very small angle at run-time.
/// Used on the world anchors to allow for fine adjustment not possible with standard Hololens placement.

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using HoloToolkit.Unity.InputModule;
using UnityEngine.UI;

public class RotationController : MonoBehaviour
{
    public TextMeshProUGUI angleText;
    public TapAnchor ta;
    public Transform rotationtransform;
    public int handlenumber;
    private float angleStep;

    public Button button;

    void Start()
    {
        angleStep = 0.5f;
       // ta.LoadAnchor();
    }

    private void Update()
    {
        transform.position = rotationtransform.position;
        transform.rotation = rotationtransform.rotation;
    }

    public void PressedButton(int buttonNumber)
    {
        switch (buttonNumber)
        {
            case 1: // rotate right
                rotationtransform.Rotate(0, -angleStep, 0);
                break;
            case 2: // rotate left
                rotationtransform.Rotate(0, angleStep, 0);
                break;
            case 3: // decrease
                angleStep -= 0.1f;
                if (angleStep < 0.1f) angleStep = 0.1f;
                angleText.text = angleStep.ToString("0.0") + "°";
                break;
            case 4: // increase
                angleStep += 0.1f;
                if (angleStep > 5f) angleStep = 5f;
                angleText.text = angleStep.ToString("0.0") + "°";
                break;
            case 6:
                Debug.Log("saved");
                ta.SaveToStore();
                button.GetComponentInChildren<TextMeshProUGUI>().text = "Saved";
                break;
        }
    }
}
