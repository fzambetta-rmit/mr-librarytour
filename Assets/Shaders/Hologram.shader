// Shader created with Shader Forge v1.38 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.38;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,cgin:,lico:1,lgpr:1,limd:1,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,imps:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:7,dpts:2,wrdp:True,dith:0,atcv:False,rfrpo:True,rfrpn:Refraction,coma:15,ufog:False,aust:True,igpj:False,qofs:0,qpre:2,rntp:3,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,atwp:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False,fsmp:False;n:type:ShaderForge.SFN_Final,id:3138,x:32824,y:32705,varname:node_3138,prsc:2|diff-9718-OUT,spec-3028-A,gloss-6256-OUT,emission-8704-OUT,clip-1241-OUT;n:type:ShaderForge.SFN_Color,id:7241,x:32211,y:32668,ptovrint:False,ptlb:Color,ptin:_Color,varname:node_7241,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:1,c3:1,c4:1;n:type:ShaderForge.SFN_Tex2d,id:3028,x:32211,y:32444,ptovrint:False,ptlb:MainTex,ptin:_MainTex,varname:node_3028,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:b3e70f03ef09f5e40abb5a526c994ef5,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Multiply,id:9718,x:32414,y:32602,varname:node_9718,prsc:2|A-3028-RGB,B-7241-RGB;n:type:ShaderForge.SFN_FragmentPosition,id:1215,x:31081,y:33205,varname:node_1215,prsc:2;n:type:ShaderForge.SFN_Time,id:8982,x:31396,y:33358,varname:node_8982,prsc:2;n:type:ShaderForge.SFN_ValueProperty,id:3173,x:31396,y:33515,ptovrint:False,ptlb:Frequency1,ptin:_Frequency1,varname:node_3173,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.5;n:type:ShaderForge.SFN_Multiply,id:5270,x:31715,y:33420,varname:node_5270,prsc:2|A-8982-T,B-3173-OUT;n:type:ShaderForge.SFN_Add,id:7914,x:31772,y:33259,varname:node_7914,prsc:2|A-5120-OUT,B-5270-OUT;n:type:ShaderForge.SFN_ValueProperty,id:6256,x:32437,y:32973,ptovrint:False,ptlb:Gloss,ptin:_Gloss,varname:node_6256,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.78;n:type:ShaderForge.SFN_Tex2d,id:3490,x:32177,y:33255,ptovrint:False,ptlb:OpacityLines,ptin:_OpacityLines,varname:node_3490,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:14e1a2765ab46144992f9b1252dfb940,ntxv:0,isnm:False|UVIN-1180-OUT;n:type:ShaderForge.SFN_Append,id:1180,x:31994,y:33194,varname:node_1180,prsc:2|A-2547-OUT,B-7914-OUT;n:type:ShaderForge.SFN_Vector1,id:2547,x:31772,y:33176,varname:node_2547,prsc:2,v1:0.5;n:type:ShaderForge.SFN_ValueProperty,id:5602,x:31396,y:33619,ptovrint:False,ptlb:Frequency2,ptin:_Frequency2,varname:node_5602,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.3;n:type:ShaderForge.SFN_Tex2d,id:9199,x:32177,y:33458,ptovrint:False,ptlb:OpacityLines2,ptin:_OpacityLines2,varname:node_9199,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:14e1a2765ab46144992f9b1252dfb940,ntxv:0,isnm:False|UVIN-5447-OUT;n:type:ShaderForge.SFN_Append,id:5447,x:31954,y:33458,varname:node_5447,prsc:2|A-2547-OUT,B-1644-OUT;n:type:ShaderForge.SFN_Multiply,id:8547,x:31565,y:33585,varname:node_8547,prsc:2|A-8982-T,B-5602-OUT;n:type:ShaderForge.SFN_Add,id:1644,x:31736,y:33585,varname:node_1644,prsc:2|A-5120-OUT,B-8547-OUT;n:type:ShaderForge.SFN_Multiply,id:1241,x:32381,y:33356,cmnt:Scrolling transparent lines,varname:node_1241,prsc:2|A-3490-R,B-9199-R;n:type:ShaderForge.SFN_ValueProperty,id:3480,x:31081,y:33134,ptovrint:False,ptlb:LineSpacing,ptin:_LineSpacing,varname:node_3480,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.3;n:type:ShaderForge.SFN_Multiply,id:5120,x:31273,y:33054,varname:node_5120,prsc:2|A-3480-OUT,B-1215-Y;n:type:ShaderForge.SFN_Tex2d,id:4190,x:31912,y:32881,ptovrint:False,ptlb:GlowLines,ptin:_GlowLines,varname:node_4190,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:14e1a2765ab46144992f9b1252dfb940,ntxv:0,isnm:False|UVIN-8397-OUT;n:type:ShaderForge.SFN_OneMinus,id:1016,x:32175,y:33087,cmnt:Glow Lines,varname:node_1016,prsc:2|IN-4190-R;n:type:ShaderForge.SFN_ValueProperty,id:5410,x:31054,y:32811,ptovrint:False,ptlb:FrequencyGlow,ptin:_FrequencyGlow,varname:node_5410,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.6;n:type:ShaderForge.SFN_Multiply,id:9631,x:31273,y:32881,varname:node_9631,prsc:2|A-5410-OUT,B-6488-T;n:type:ShaderForge.SFN_Add,id:7897,x:31478,y:32881,varname:node_7897,prsc:2|A-9631-OUT,B-5120-OUT;n:type:ShaderForge.SFN_Append,id:8397,x:31729,y:32881,varname:node_8397,prsc:2|A-2547-OUT,B-7897-OUT;n:type:ShaderForge.SFN_Multiply,id:8704,x:32402,y:33087,varname:node_8704,prsc:2|A-2429-RGB,B-1016-OUT;n:type:ShaderForge.SFN_Color,id:2429,x:32175,y:32915,ptovrint:False,ptlb:GlowColor,ptin:_GlowColor,varname:node_2429,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.03676468,c2:1,c3:0.880426,c4:1;n:type:ShaderForge.SFN_Time,id:6488,x:31054,y:32881,varname:node_6488,prsc:2;proporder:3028-7241-6256-3490-9199-3173-5602-3480-2429-4190-5410;pass:END;sub:END;*/

Shader "Custom/Hologram" {
    Properties {
        _MainTex ("MainTex", 2D) = "white" {}
        _Color ("Color", Color) = (1,1,1,1)
        _Gloss ("Gloss", Float ) = 0.78
        _OpacityLines ("OpacityLines", 2D) = "white" {}
        _OpacityLines2 ("OpacityLines2", 2D) = "white" {}
        _Frequency1 ("Frequency1", Float ) = 0.5
        _Frequency2 ("Frequency2", Float ) = 0.3
        _LineSpacing ("LineSpacing", Float ) = 0.3
        _GlowColor ("GlowColor", Color) = (0.03676468,1,0.880426,1)
        _GlowLines ("GlowLines", 2D) = "white" {}
        _FrequencyGlow ("FrequencyGlow", Float ) = 0.6
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "Queue"="AlphaTest"
            "RenderType"="TransparentCutout"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend One OneMinusSrcAlpha
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma only_renderers d3d9 d3d11 glcore gles gles3 metal 
            #pragma target 3.0
            uniform float4 _LightColor0;
            uniform float4 _Color;
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform float _Frequency1;
            uniform float _Gloss;
            uniform sampler2D _OpacityLines; uniform float4 _OpacityLines_ST;
            uniform float _Frequency2;
            uniform sampler2D _OpacityLines2; uniform float4 _OpacityLines2_ST;
            uniform float _LineSpacing;
            uniform sampler2D _GlowLines; uniform float4 _GlowLines_ST;
            uniform float _FrequencyGlow;
            uniform float4 _GlowColor;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                LIGHTING_COORDS(3,4)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = UnityObjectToClipPos( v.vertex );
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
                float node_2547 = 0.5;
                float node_5120 = (_LineSpacing*i.posWorld.g);
                float4 node_8982 = _Time;
                float2 node_1180 = float2(node_2547,(node_5120+(node_8982.g*_Frequency1)));
                float4 _OpacityLines_var = tex2D(_OpacityLines,TRANSFORM_TEX(node_1180, _OpacityLines));
                float2 node_5447 = float2(node_2547,(node_5120+(node_8982.g*_Frequency2)));
                float4 _OpacityLines2_var = tex2D(_OpacityLines2,TRANSFORM_TEX(node_5447, _OpacityLines2));
                clip((_OpacityLines_var.r*_OpacityLines2_var.r) - 0.5);
                float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
                float3 lightColor = _LightColor0.rgb;
                float3 halfDirection = normalize(viewDirection+lightDirection);
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float3 attenColor = attenuation * _LightColor0.xyz;
///////// Gloss:
                float gloss = _Gloss;
                float specPow = exp2( gloss * 10.0 + 1.0 );
////// Specular:
                float NdotL = saturate(dot( normalDirection, lightDirection ));
                float4 _MainTex_var = tex2D(_MainTex,TRANSFORM_TEX(i.uv0, _MainTex));
                float3 specularColor = float3(_MainTex_var.a,_MainTex_var.a,_MainTex_var.a);
                float3 directSpecular = attenColor * pow(max(0,dot(halfDirection,normalDirection)),specPow)*specularColor;
                float3 specular = directSpecular;
/////// Diffuse:
                NdotL = max(0.0,dot( normalDirection, lightDirection ));
                float3 directDiffuse = max( 0.0, NdotL) * attenColor;
                float3 indirectDiffuse = float3(0,0,0);
                indirectDiffuse += UNITY_LIGHTMODEL_AMBIENT.rgb; // Ambient Light
                float3 diffuseColor = (_MainTex_var.rgb*_Color.rgb);
                float3 diffuse = (directDiffuse + indirectDiffuse) * diffuseColor;
////// Emissive:
                float4 node_6488 = _Time;
                float2 node_8397 = float2(node_2547,((_FrequencyGlow*node_6488.g)+node_5120));
                float4 _GlowLines_var = tex2D(_GlowLines,TRANSFORM_TEX(node_8397, _GlowLines));
                float3 emissive = (_GlowColor.rgb*(1.0 - _GlowLines_var.r));
/// Final Color:
                float3 finalColor = diffuse + specular + emissive;
                return fixed4(finalColor,1);
            }
            ENDCG
        }
        Pass {
            Name "FORWARD_DELTA"
            Tags {
                "LightMode"="ForwardAdd"
            }
            Blend One One
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDADD
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #pragma multi_compile_fwdadd_fullshadows
            #pragma only_renderers d3d9 d3d11 glcore gles gles3 metal 
            #pragma target 3.0
            uniform float4 _LightColor0;
            uniform float4 _Color;
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform float _Frequency1;
            uniform float _Gloss;
            uniform sampler2D _OpacityLines; uniform float4 _OpacityLines_ST;
            uniform float _Frequency2;
            uniform sampler2D _OpacityLines2; uniform float4 _OpacityLines2_ST;
            uniform float _LineSpacing;
            uniform sampler2D _GlowLines; uniform float4 _GlowLines_ST;
            uniform float _FrequencyGlow;
            uniform float4 _GlowColor;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                LIGHTING_COORDS(3,4)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = UnityObjectToClipPos( v.vertex );
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
                float node_2547 = 0.5;
                float node_5120 = (_LineSpacing*i.posWorld.g);
                float4 node_8982 = _Time;
                float2 node_1180 = float2(node_2547,(node_5120+(node_8982.g*_Frequency1)));
                float4 _OpacityLines_var = tex2D(_OpacityLines,TRANSFORM_TEX(node_1180, _OpacityLines));
                float2 node_5447 = float2(node_2547,(node_5120+(node_8982.g*_Frequency2)));
                float4 _OpacityLines2_var = tex2D(_OpacityLines2,TRANSFORM_TEX(node_5447, _OpacityLines2));
                clip((_OpacityLines_var.r*_OpacityLines2_var.r) - 0.5);
                float3 lightDirection = normalize(lerp(_WorldSpaceLightPos0.xyz, _WorldSpaceLightPos0.xyz - i.posWorld.xyz,_WorldSpaceLightPos0.w));
                float3 lightColor = _LightColor0.rgb;
                float3 halfDirection = normalize(viewDirection+lightDirection);
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float3 attenColor = attenuation * _LightColor0.xyz;
///////// Gloss:
                float gloss = _Gloss;
                float specPow = exp2( gloss * 10.0 + 1.0 );
////// Specular:
                float NdotL = saturate(dot( normalDirection, lightDirection ));
                float4 _MainTex_var = tex2D(_MainTex,TRANSFORM_TEX(i.uv0, _MainTex));
                float3 specularColor = float3(_MainTex_var.a,_MainTex_var.a,_MainTex_var.a);
                float3 directSpecular = attenColor * pow(max(0,dot(halfDirection,normalDirection)),specPow)*specularColor;
                float3 specular = directSpecular;
/////// Diffuse:
                NdotL = max(0.0,dot( normalDirection, lightDirection ));
                float3 directDiffuse = max( 0.0, NdotL) * attenColor;
                float3 diffuseColor = (_MainTex_var.rgb*_Color.rgb);
                float3 diffuse = directDiffuse * diffuseColor;
/// Final Color:
                float3 finalColor = diffuse + specular;
                return fixed4(finalColor * 1,0);
            }
            ENDCG
        }
        Pass {
            Name "ShadowCaster"
            Tags {
                "LightMode"="ShadowCaster"
            }
            Offset 1, 1
            Cull Back
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_SHADOWCASTER
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma only_renderers d3d9 d3d11 glcore gles gles3 metal 
            #pragma target 3.0
            uniform float _Frequency1;
            uniform sampler2D _OpacityLines; uniform float4 _OpacityLines_ST;
            uniform float _Frequency2;
            uniform sampler2D _OpacityLines2; uniform float4 _OpacityLines2_ST;
            uniform float _LineSpacing;
            struct VertexInput {
                float4 vertex : POSITION;
            };
            struct VertexOutput {
                V2F_SHADOW_CASTER;
                float4 posWorld : TEXCOORD1;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = UnityObjectToClipPos( v.vertex );
                TRANSFER_SHADOW_CASTER(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                float node_2547 = 0.5;
                float node_5120 = (_LineSpacing*i.posWorld.g);
                float4 node_8982 = _Time;
                float2 node_1180 = float2(node_2547,(node_5120+(node_8982.g*_Frequency1)));
                float4 _OpacityLines_var = tex2D(_OpacityLines,TRANSFORM_TEX(node_1180, _OpacityLines));
                float2 node_5447 = float2(node_2547,(node_5120+(node_8982.g*_Frequency2)));
                float4 _OpacityLines2_var = tex2D(_OpacityLines2,TRANSFORM_TEX(node_5447, _OpacityLines2));
                clip((_OpacityLines_var.r*_OpacityLines2_var.r) - 0.5);
                SHADOW_CASTER_FRAGMENT(i)
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
