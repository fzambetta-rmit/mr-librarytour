// Shader created with Shader Forge v1.38 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.38;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,cgin:,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,imps:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:2,bsrc:0,bdst:6,dpts:2,wrdp:False,dith:0,atcv:False,rfrpo:True,rfrpn:Refraction,coma:15,ufog:False,aust:True,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,atwp:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False,fsmp:False;n:type:ShaderForge.SFN_Final,id:3138,x:32786,y:32702,varname:node_3138,prsc:2|emission-4444-OUT;n:type:ShaderForge.SFN_Color,id:7241,x:32234,y:32737,ptovrint:False,ptlb:Color,ptin:_Color,varname:node_7241,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.07843138,c2:0.3921569,c3:0.7843137,c4:1;n:type:ShaderForge.SFN_Tex2d,id:4611,x:32234,y:32952,ptovrint:False,ptlb:Texture,ptin:_Texture,varname:node_4611,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False|UVIN-9103-OUT;n:type:ShaderForge.SFN_Multiply,id:4444,x:32539,y:32850,varname:node_4444,prsc:2|A-7241-RGB,B-7915-OUT;n:type:ShaderForge.SFN_TexCoord,id:5041,x:31639,y:33095,varname:node_5041,prsc:2,uv:0,uaff:False;n:type:ShaderForge.SFN_Time,id:8163,x:31394,y:32687,varname:node_8163,prsc:2;n:type:ShaderForge.SFN_Append,id:9103,x:31996,y:32952,varname:node_9103,prsc:2|A-5791-OUT,B-5041-V;n:type:ShaderForge.SFN_Add,id:5791,x:31837,y:32803,varname:node_5791,prsc:2|A-3195-OUT,B-5041-U;n:type:ShaderForge.SFN_Multiply,id:3195,x:31613,y:32837,varname:node_3195,prsc:2|A-8163-T,B-6109-OUT;n:type:ShaderForge.SFN_ValueProperty,id:6109,x:31373,y:32871,ptovrint:False,ptlb:ScrollSpeed,ptin:_ScrollSpeed,varname:node_6109,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.1;n:type:ShaderForge.SFN_Fresnel,id:8420,x:32193,y:33172,varname:node_8420,prsc:2|EXP-5039-OUT;n:type:ShaderForge.SFN_Add,id:7915,x:32454,y:33070,varname:node_7915,prsc:2|A-4611-RGB,B-8032-OUT;n:type:ShaderForge.SFN_Slider,id:5039,x:31788,y:33211,ptovrint:False,ptlb:Fresnel,ptin:_Fresnel,varname:node_5039,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:3.632479,max:5;n:type:ShaderForge.SFN_TexCoord,id:6746,x:32160,y:33362,varname:node_6746,prsc:2,uv:0,uaff:False;n:type:ShaderForge.SFN_Multiply,id:8032,x:32471,y:33269,varname:node_8032,prsc:2|A-8420-OUT,B-1418-OUT;n:type:ShaderForge.SFN_OneMinus,id:1418,x:32382,y:33449,varname:node_1418,prsc:2|IN-6746-V;proporder:7241-4611-6109-5039;pass:END;sub:END;*/

Shader "Custom/HologramLight" {
    Properties {
        _Color ("Color", Color) = (0.07843138,0.3921569,0.7843137,1)
        _Texture ("Texture", 2D) = "white" {}
        _ScrollSpeed ("ScrollSpeed", Float ) = 0.1
        _Fresnel ("Fresnel", Range(0, 5)) = 3.632479
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend One OneMinusSrcColor
            Cull Off
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma only_renderers d3d9 d3d11 glcore gles gles3 metal 
            #pragma target 3.0
            uniform float4 _Color;
            uniform sampler2D _Texture; uniform float4 _Texture_ST;
            uniform float _ScrollSpeed;
            uniform float _Fresnel;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = UnityObjectToClipPos( v.vertex );
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
                i.normalDir = normalize(i.normalDir);
                i.normalDir *= faceSign;
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
////// Lighting:
////// Emissive:
                float4 node_8163 = _Time;
                float2 node_9103 = float2(((node_8163.g*_ScrollSpeed)+i.uv0.r),i.uv0.g);
                float4 _Texture_var = tex2D(_Texture,TRANSFORM_TEX(node_9103, _Texture));
                float3 emissive = (_Color.rgb*(_Texture_var.rgb+(pow(1.0-max(0,dot(normalDirection, viewDirection)),_Fresnel)*(1.0 - i.uv0.g))));
                float3 finalColor = emissive;
                return fixed4(finalColor,1);
            }
            ENDCG
        }
        Pass {
            Name "ShadowCaster"
            Tags {
                "LightMode"="ShadowCaster"
            }
            Offset 1, 1
            Cull Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_SHADOWCASTER
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma only_renderers d3d9 d3d11 glcore gles gles3 metal 
            #pragma target 3.0
            struct VertexInput {
                float4 vertex : POSITION;
            };
            struct VertexOutput {
                V2F_SHADOW_CASTER;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.pos = UnityObjectToClipPos( v.vertex );
                TRANSFER_SHADOW_CASTER(o)
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
                SHADOW_CASTER_FRAGMENT(i)
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
