// Shader created with Shader Forge v1.38 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.38;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,cgin:,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,imps:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:2,bsrc:0,bdst:0,dpts:2,wrdp:False,dith:0,atcv:False,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,atwp:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False,fsmp:False;n:type:ShaderForge.SFN_Final,id:7814,x:32719,y:32712,varname:node_7814,prsc:2|emission-1179-OUT;n:type:ShaderForge.SFN_Tex2d,id:5352,x:32280,y:32691,ptovrint:False,ptlb:Texture,ptin:_Texture,varname:node_5352,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:0307985bf05274042984a57c31341b81,ntxv:0,isnm:False|UVIN-6437-OUT;n:type:ShaderForge.SFN_Tex2d,id:1257,x:32280,y:32883,ptovrint:False,ptlb:Opacity,ptin:_Opacity,varname:node_1257,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:0307985bf05274042984a57c31341b81,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Multiply,id:1179,x:32512,y:32813,varname:node_1179,prsc:2|A-5352-RGB,B-1257-A,C-4396-RGB;n:type:ShaderForge.SFN_TexCoord,id:3929,x:31517,y:32634,varname:node_3929,prsc:2,uv:0,uaff:False;n:type:ShaderForge.SFN_ValueProperty,id:5521,x:31133,y:32921,ptovrint:False,ptlb:ScrollV,ptin:_ScrollV,varname:node_5521,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1;n:type:ShaderForge.SFN_Time,id:6072,x:31133,y:32705,varname:node_6072,prsc:2;n:type:ShaderForge.SFN_Multiply,id:7591,x:31326,y:32768,varname:node_7591,prsc:2|A-6072-T,B-5521-OUT;n:type:ShaderForge.SFN_Frac,id:5939,x:31885,y:32771,varname:node_5939,prsc:2|IN-4427-OUT;n:type:ShaderForge.SFN_Append,id:6437,x:32065,y:32691,varname:node_6437,prsc:2|A-7932-OUT,B-5939-OUT;n:type:ShaderForge.SFN_Add,id:4427,x:31708,y:32771,varname:node_4427,prsc:2|A-3929-V,B-7591-OUT;n:type:ShaderForge.SFN_Color,id:4396,x:32280,y:33079,ptovrint:False,ptlb:ColorTint,ptin:_ColorTint,varname:node_4396,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.4338235,c2:0.6251522,c3:1,c4:1;n:type:ShaderForge.SFN_ValueProperty,id:9626,x:31133,y:32561,ptovrint:False,ptlb:ScrollU,ptin:_ScrollU,varname:node_9626,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.1;n:type:ShaderForge.SFN_Multiply,id:1759,x:31326,y:32617,varname:node_1759,prsc:2|A-9626-OUT,B-6072-T;n:type:ShaderForge.SFN_Add,id:3843,x:31708,y:32522,varname:node_3843,prsc:2|A-1759-OUT,B-3929-U;n:type:ShaderForge.SFN_Frac,id:7932,x:31885,y:32522,varname:node_7932,prsc:2|IN-3843-OUT;proporder:5352-4396-1257-9626-5521;pass:END;sub:END;*/

Shader "Custom/TransporterRain" {
    Properties {
        _Texture ("Texture", 2D) = "white" {}
        _ColorTint ("ColorTint", Color) = (0.4338235,0.6251522,1,1)
        _Opacity ("Opacity", 2D) = "white" {}
        _ScrollU ("ScrollU", Float ) = 0.1
        _ScrollV ("ScrollV", Float ) = 1
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        LOD 200
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend One One
            Cull Off
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma multi_compile_fog
            #pragma only_renderers d3d9 d3d11 glcore gles gles3 metal 
            #pragma target 3.0
            uniform sampler2D _Texture; uniform float4 _Texture_ST;
            uniform sampler2D _Opacity; uniform float4 _Opacity_ST;
            uniform float _ScrollV;
            uniform float4 _ColorTint;
            uniform float _ScrollU;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                UNITY_FOG_COORDS(1)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.pos = UnityObjectToClipPos( v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
////// Lighting:
////// Emissive:
                float4 node_6072 = _Time;
                float2 node_6437 = float2(frac(((_ScrollU*node_6072.g)+i.uv0.r)),frac((i.uv0.g+(node_6072.g*_ScrollV))));
                float4 _Texture_var = tex2D(_Texture,TRANSFORM_TEX(node_6437, _Texture));
                float4 _Opacity_var = tex2D(_Opacity,TRANSFORM_TEX(i.uv0, _Opacity));
                float3 emissive = (_Texture_var.rgb*_Opacity_var.a*_ColorTint.rgb);
                float3 finalColor = emissive;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "ShadowCaster"
            Tags {
                "LightMode"="ShadowCaster"
            }
            Offset 1, 1
            Cull Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_SHADOWCASTER
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma multi_compile_fog
            #pragma only_renderers d3d9 d3d11 glcore gles gles3 metal 
            #pragma target 3.0
            struct VertexInput {
                float4 vertex : POSITION;
            };
            struct VertexOutput {
                V2F_SHADOW_CASTER;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.pos = UnityObjectToClipPos( v.vertex );
                TRANSFER_SHADOW_CASTER(o)
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
                SHADOW_CASTER_FRAGMENT(i)
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
