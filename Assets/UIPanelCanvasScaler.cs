﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class UIPanelCanvasScaler : MonoBehaviour {
	public UnityEngine.UI.GridLayoutGroup gridLayout;
	//public Canvas rootCanvas;
	public RectTransform rootTransform;

	void Awake() {
		if (gridLayout == null)
			gridLayout = GetComponent<UnityEngine.UI.GridLayoutGroup> ();

		/*if (rootCanvas == null)	
			rootCanvas = gameObject.GetComponentInParent<Canvas> ().rootCanvas;*/
		
		//rootTransform = rootCanvas.GetComponent<RectTransform> ();
	}

	// Use this for initialization
	void Start () {
		UpdateCellSize ();
	}

	void UpdateCellSize()
	{
		if (gridLayout != null) 
		{
			float panelWidth = rootTransform.rect.width;// * rootTransform.localScale.x;
			float panelHeight = rootTransform.rect.height;// * rootTransform.localScale.y;
			gridLayout.cellSize = new Vector2 (panelWidth, panelHeight);
		}
	}

	#if UNITY_EDITOR
	// Update is called once per frame
	void Update () 
	{
		UpdateCellSize ();
	}

	#endif
}
