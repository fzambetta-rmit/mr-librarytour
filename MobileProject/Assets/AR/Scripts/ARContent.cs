﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu()]
public class ARLink : ScriptableObject {
	public GameObject targetPrefab;
	public GameObject contentPrefab; // only 1 prefab for now.

}
