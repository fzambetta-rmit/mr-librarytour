﻿using UnityEngine;
using UnityEngine.XR.WSA;
using UnityEngine.XR.WSA.Persistence;

public class AnchorObject : MonoBehaviour 
{

    private bool loadAnchorAtStartup = true;
    protected WorldAnchorStore store = null;
    protected GameObject sphere = null;


    protected void StoreLoaded(WorldAnchorStore store)
    {
        this.store = store;
        if (loadAnchorAtStartup)
            LoadAnchor();
    }

    protected void Update()
    {
        // s key to save object anchor
        if (Input.GetKeyDown("s"))
            SaveAnchor();


        // c key to clear object anchor, thus allowing to change its position from the Unity editor
        if (Input.GetKeyDown("c"))
        {
            WorldAnchor anchor = gameObject.GetComponent<WorldAnchor>();
            if (anchor != null)
                DestroyImmediate(anchor);

            SetColor(sphere, Color.white);
        }
    }


    protected void SetColor(GameObject obj, Color color)
    {
        Renderer renderer = obj.GetComponent<Renderer>();
        if (renderer == null)
            return;

        renderer.material.color = color;
    }

    protected void Start()
    {
        sphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        sphere.transform.parent = transform;
        SetColor(sphere, Color.white);
        WorldAnchorStore.GetAsync(StoreLoaded);
    }

    public bool SaveAnchor()
    {
        //do the cleanup: if an anchor already exists, destroy it (from the gameobject and the store)
        WorldAnchor anchor = gameObject.GetComponent<WorldAnchor>();

        if (anchor != null)
            DestroyImmediate(anchor);

        if (store != null)
        {
            store.Delete(gameObject.name);

            //add a game anchor to current object
            anchor = gameObject.AddComponent<WorldAnchor>();

            //save the anchor to the store
            if (!store.Save(gameObject.name, anchor))
            {
                Debug.LogWarning(string.Format("Anchor {0} NOT saved", gameObject.name));
                SetColor(sphere, Color.blue);
            }
            else
            {
                Debug.Log(string.Format("Anchor {0} successfully saved", gameObject.name));
                SetColor(sphere, Color.green);
                return true; //return true, because everything went all right
            }
        }
        else
            Debug.LogWarning(string.Format("Anchor {0} NOT saved because there's no access to the store", gameObject.name));

        //if we're here, we failed somehow
        return false;
    }

    public bool LoadAnchor()
    {
        //load the anchor of this object if available

        if (store != null)
        {
            //get current world anchor (if it doesn't exist, add it)
            WorldAnchor anchor = gameObject.GetComponent<WorldAnchor>();

            if (anchor != null)
                DestroyImmediate(anchor);

            //load the anchor from the store
            if (!store.Load(gameObject.name, gameObject))
            {
                Debug.LogWarning(string.Format("Anchor {0} NOT loaded", gameObject.name));
                SetColor(sphere, Color.red);
            }
            else
            {
                Debug.Log(string.Format("Anchor {0} successfully loaded", gameObject.name));
                SetColor(sphere, Color.green);
                return true; //return true, because everything went all right
            }
        }
        else
        {
            Debug.LogWarning(string.Format("Anchor {0} NOT loaded because there's no access to the store", gameObject.name));
            SetColor(sphere, Color.yellow);
        }

        //if we're here, we failed somehow
        return false;
    }
}