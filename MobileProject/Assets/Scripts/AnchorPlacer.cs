using UnityEngine;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Assertions;
using UnityEngine.XR.WSA.Persistence;
using System.IO;


public class AnchorPlacer : MonoBehaviour
{

    private bool loadAnchorAtStartup = true;
    private byte[] exportBytes;

    protected WorldAnchorStore store = null;
    protected GameObject sphere = null;


    private void OnExportDataAvailable(byte[] data)
    {
      // Send the bytes to the client.  Data may also be buffered.
      exportBytes = exportBytes.Concat(data).ToArray();
    
      Debug.Log("Export bytes count: " + exportBytes.Length);
    }




    protected void StoreLoaded(WorldAnchorStore store)
    {
      this.store = store;
    	LoadAnchor();
    }


    protected void Update()
    {
      if(Input.GetKeyDown("p"))
    	SaveAnchor();
    
      
      if(Input.GetKeyDown("o"))
      {
      	UnityEngine.XR.WSA.WorldAnchor anchor = gameObject.GetComponent<UnityEngine.XR.WSA.WorldAnchor>();
    	if(anchor != null)
    		DestroyImmediate(anchor);
    
    	SetColor(sphere, Color.white);
      }
    
    
      if(Input.GetKeyDown("x"))
    	SaveAnchorDataTBatch();
    
    }


    protected void SetColor(GameObject obj,Color color)
    {
      Renderer renderer = obj.GetComponent<Renderer>();
      if(renderer == null)
    	return;
    
      renderer.material.color = color;
    }


    protected void Start()
    {
      sphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
      sphere.transform.parent = transform;
    
      SetColor(sphere, Color.white);
      WorldAnchorStore.GetAsync(StoreLoaded);
    }


    protected void OnExportComplete(UnityEngine.XR.WSA.Sharing.SerializationCompletionReason completionReason)
    {
       if (completionReason != UnityEngine.XR.WSA.Sharing.SerializationCompletionReason.Succeeded)
       {
    	Debug.Log("Failed serialization: " + completionReason.ToString());
       }
       else
       {
            try {
    		System.IO.File.WriteAllBytes(Path.Combine(Application.dataPath, "/AnchorBatch.dat"), exportBytes);
    		Debug.Log("Serialization successful");
    	}
    	catch(Exception ex)
    	{
    		Debug.LogError("Failed serialization: " + ex.ToString());
    	}
       }
    
    }


    protected void SaveAnchorDataTBatch()
    { 
      if(gameObject.GetComponent<UnityEngine.XR.WSA.WorldAnchor>() == null)
      {
    	Debug.Log("no anchors to serialize");
    	return;
      }
    
    //#if !UNITY_EDITOR
      exportBytes = new byte[0];
    
      Debug.Log(String.Format("Saving anchor data batch to file: {0}", Path.Combine(Application.dataPath, "/AnchorBatch.dat")));
    
      UnityEngine.XR.WSA.Sharing.WorldAnchorTransferBatch transferBatch = new UnityEngine.XR.WSA.Sharing.WorldAnchorTransferBatch();
       
      transferBatch.AddWorldAnchor(gameObject.name, gameObject.GetComponent<UnityEngine.XR.WSA.WorldAnchor>());
    
      UnityEngine.XR.WSA.Sharing.WorldAnchorTransferBatch.ExportAsync(transferBatch, OnExportDataAvailable, OnExportComplete);
    //#else
      //Debug.LogWarning("Anchor Transfer Batch is not allowed in Play mode!");
    //#endif
    }




    public bool SaveAnchor()
    {
        Debug.Log("Saving");
        //do the cleanup: if an anchor already exists, destroy it (from the gameobject and the store)
        UnityEngine.XR.WSA.WorldAnchor anchor = gameObject.GetComponent<UnityEngine.XR.WSA.WorldAnchor>();
    
      if(anchor != null)
    	DestroyImmediate(anchor);
    
      if(store != null)
      {  		
    	store.Delete(gameObject.name);
    
    	//add a game anchor to current object
    	anchor = gameObject.AddComponent<UnityEngine.XR.WSA.WorldAnchor>();
    
    	//save the anchor to the store
    	if(!store.Save(gameObject.name, anchor))
    	{
    		Debug.LogWarning(string.Format("Anchor {0} NOT saved", gameObject.name));
    		SetColor(sphere, Color.blue);
      	}
    	else
    	{
    		Debug.Log(string.Format("Anchor {0} successfully saved", gameObject.name));
    		SetColor(sphere, Color.green);
    		return true; //return true, because everything went all right
    	}
      }
      else
      	Debug.LogWarning(string.Format("Anchor {0} NOT saved because there's no access to the store", gameObject.name));
    
      //if we're here, we failed somehow
      return false;
    }


    public bool LoadAnchor()
    {
      //load the anchor of this object if available
    
      if(store != null)
      {  		
      	//get current world anchor (if it doesn't exist, add it)
    	UnityEngine.XR.WSA.WorldAnchor anchor = gameObject.GetComponent<UnityEngine.XR.WSA.WorldAnchor>();
    
    	if(anchor != null)
    		DestroyImmediate(anchor);
    
    	//load the anchor from the store
    	if(!store.Load(gameObject.name, gameObject))
    	{
    		Debug.LogWarning(string.Format("Anchor {0} NOT loaded", gameObject.name));
    		SetColor(sphere, Color.red);
      	}
    	else
    	{
    		Debug.Log(string.Format("Anchor {0} successfully loaded", gameObject.name));
    		SetColor(sphere, Color.green);
    		return true; //return true, because everything went all right
    	}
      }
      else
      {
      	Debug.LogWarning(string.Format("Anchor {0} NOT loaded because there's no access to the store", gameObject.name));
    	SetColor(sphere, Color.yellow);
      }
    
      //if we're here, we failed somehow
      return false;
    }


}
