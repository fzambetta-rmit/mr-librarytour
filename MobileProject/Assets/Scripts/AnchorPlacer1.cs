﻿using UnityEngine;
using UnityEngine.XR.WSA;
using UnityEngine.XR.WSA.Persistence;
using UnityEngine.XR.WSA.Sharing;
using System;
using System.Linq;
using System.IO;


public class AnchorPlacer1 : MonoBehaviour
{

    private bool loadAnchorAtStartup = true;
    private byte[] exportBytes;

    protected WorldAnchorStore store = null;
    protected GameObject sphere = null;


    private void OnExportDataAvailable(byte[] data)
    {
        // Send the bytes to the client.  Data may also be buffered.
        exportBytes = exportBytes.Concat(data).ToArray();

        Debug.Log("Export bytes count: " + exportBytes.Length);
    }




    protected void StoreLoaded(WorldAnchorStore store)
    {
        this.store = store;
    }


    protected void Update()
    {
        if (Input.GetKeyDown("s"))
            SaveAnchor();


        if (Input.GetKeyDown("c"))
        {
            WorldAnchor anchor = gameObject.GetComponent<WorldAnchor>();
            if (anchor != null)
                DestroyImmediate(anchor);

            SetColor(sphere, Color.white);
        }


        if (Input.GetKeyDown("x"))
            SaveAnchorDataTBatch();

    }


    protected void SetColor(GameObject obj, Color color)
    {
        Renderer renderer = obj.GetComponent<Renderer>();
        if (renderer == null)
            return;

        renderer.material.color = color;
    }


    protected void Start()
    {
        sphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        sphere.transform.parent = transform;

        SetColor(sphere, Color.white);
        WorldAnchorStore.GetAsync(StoreLoaded);
    }


    protected void OnExportComplete(SerializationCompletionReason completionReason)
    {
        if (completionReason != SerializationCompletionReason.Succeeded)
        {
            Debug.Log("Failed serialization: " + completionReason.ToString());
        }
        else
        {
            try
            {
                System.IO.File.WriteAllBytes(Path.Combine(Application.dataPath, "/AnchorBatch.dat"), exportBytes);
                Debug.Log("Serialization successful");
            }
            catch (Exception ex)
            {
                Debug.LogError("Failed serialization: " + ex.ToString());
            }
        }

    }


    protected void SaveAnchorDataTBatch()
    {
        if (gameObject.GetComponent<WorldAnchor>() == null)
        {
            Debug.Log("no anchors to serialize");
            return;
        }

        //#if !UNITY_EDITOR
        exportBytes = new byte[0];

        Debug.Log(String.Format("Saving anchor data batch to file: {0}", Path.Combine(Application.dataPath, "/AnchorBatch.dat")));

        WorldAnchorTransferBatch transferBatch = new WorldAnchorTransferBatch();

        transferBatch.AddWorldAnchor(gameObject.name, gameObject.GetComponent<WorldAnchor>());

        WorldAnchorTransferBatch.ExportAsync(transferBatch, OnExportDataAvailable, OnExportComplete);
        //#else
        //Debug.LogWarning("Anchor Transfer Batch is not allowed in Play mode!");
        //#endif
    }




    public bool SaveAnchor()
    {
        //do the cleanup: if an anchor already exists, destroy it (from the gameobject and the store)
        WorldAnchor anchor = gameObject.GetComponent<WorldAnchor>();

        if (anchor != null)
            DestroyImmediate(anchor);

        if (store != null)
        {
            store.Delete(gameObject.name);

            //add a game anchor to current object
            anchor = gameObject.AddComponent<WorldAnchor>();

            //save the anchor to the store
            if (!store.Save(gameObject.name, anchor))
            {
                Debug.LogWarning(string.Format("Anchor {0} NOT saved", gameObject.name));
                SetColor(sphere, Color.blue);
            }
            else
            {
                Debug.Log(string.Format("Anchor {0} successfully saved", gameObject.name));
                SetColor(sphere, Color.green);
                return true; //return true, because everything went all right
            }
        }
        else
            Debug.LogWarning(string.Format("Anchor {0} NOT saved because there's no access to the store", gameObject.name));

        //if we're here, we failed somehow
        return false;
    }





}
