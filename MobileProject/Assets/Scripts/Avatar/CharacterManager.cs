﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using TMPro;

public class CharacterManager : MonoBehaviour
{
	public Animator myAnimator;
	public SpeechBubble speechBubble;
	public GameObject myCanvas, hexButtons;
	public FlashingTrail[] trails;
	public TextMeshProUGUI buttonText;
	public AudioClip[] audioClips;
	public AudioSource owlbotAudio;
	
	public struct ScriptElement
	{
		public string title;
		public string body;
		public string url;
	}

	public bool playAudio;
	[Tooltip("When true the app progresses without any user input")]
	public bool automaticProgress;
	public float autoProgressDelay;
	public enum Platforms { mobile, hololens };

	private int scriptIndex;
	private string dataFilename = "interactionScript.json"; // relative to /StreamingAssets
	private enum Gesture { wave, pointLeft, pointRight, question, helpDesk, groupWork, laptop, openingHours, idleFlap, idleLook, whereAreTheBooks, returningBooks, readingBook, toilets, groupStudy, printing, drinkingFountain, returnChute };

	private ScriptElement[] interactionScript;
	private const int scriptLength = 29;
	private List<int> posterContent; // which interaction script entries are needed for this specific poster
	private int posterNumber;

	public static Platforms currentPlatform = Platforms.hololens; // TODO: read this from the hardware

	public void Initialize(int _posterNumber)
	{
		posterNumber = _posterNumber;
		// fill script interactionScript
		interactionScript = new ScriptElement[scriptLength];
		posterContent = new List<int>();

		for (int i = 0; i < scriptLength; i++) {
			interactionScript[1].url = ""; // initialize URLs to blank, they're only shown on mobile
		}

		#region Introduction
		// HOLOLENS ONLY
		interactionScript[0].title = "Welcome";
		interactionScript[0].body = "To begin the tour look at the word Continue, then reach out with your index finger and tap downwards as if clicking a mouse.";

		// HOLOLENS ONLY
		interactionScript[1].title = "Welcome";
		interactionScript[1].body = "RMIT University acknowledges the Wurundjeri people of the Kulin Nations as the traditional owners of the land on which the University stands. RMIT University respectfully recognises Elders both past and present.";

		interactionScript[2].title = "Welcome";
		interactionScript[2].body = "Hi, I'm Owlbot. I'll be showing you around.";

		interactionScript[3].title = "Welcome";
		interactionScript[3].body = "Welcome to Swanston Library, the heart of the city campus. The Library is a great place to study, to meet your friends and to get help with your assignments and research.";

		interactionScript[4].title = "Opening hours";
		interactionScript[4].body = "Wow, the Library is open every week day from 8am to midnight. It is also open from 10am to 5pm on weekends and most public holidays, with extra opening hours at exam time.";
		interactionScript[4].url = "http://www1.rmit.edu.au/browse;ID=6g8vd17991c61";

		interactionScript[5].title = "Where can I get help?";
		interactionScript[5].body = "You can start by asking a librarian at one of the service desks. They are located on Levels 5 and 6. You can also use our online chat service to email a librarian.";
		interactionScript[5].url = "http://www1.rmit.edu.au/browse;ID=0n2yedepkmk";

		interactionScript[6].title = "Where can I get help?";
		interactionScript[6].body = "If you have an IT question, you can ask our IT help desk staff at the service desk on Level 6.";
		#endregion

		#region Study and Learning Centre
		interactionScript[7].title = "What is the Study and Learning Centre Drop-in?";
		interactionScript[7].body = "SLC Drop-in is a friendly space where you can get expert help with your uni work.";

		interactionScript[8].title = "What is the Study and Learning Centre Drop-in?";
		interactionScript[8].body = "Get help with your assignments, learn study skills and tips, and get help with maths and physics problems. Experienced learning advisors are ready to assist you in a relaxed environment. They can give you feedback on your work, though they don't proofread it.";

		interactionScript[9].title = "What is the Study and Learning Centre Drop-in?";
		interactionScript[9].body = "There's no need to make an appointment and you can get help throughout the semester, whenever you need it.";

		//	MOBILE ONLY
		interactionScript[10].title = "What is the Study and Learning Centre Drop-in?";
		interactionScript[10].body = "Click here to find out more about opening hours, workshops and the English Ready program.";
		interactionScript[10].url = "https://www.rmit.edu.au/students/study-support/study-and-learning-centre";

		//	MOBILE ONLY
		interactionScript[11].title = "Learning Lab";
		interactionScript[11].body = "You can also get study help online with Learning Lab";
		interactionScript[11].url = "https://emedia.rmit.edu.au/learninglab/welcome";
		#endregion

		#region Computers
		interactionScript[12].title = "Computers";
		interactionScript[12].body = "Do you know there are over 100 computers that you can book in the Library?";

		interactionScript[13].title = "Computers";
		interactionScript[13].body = "Computers are located throughout the Library. They're very popular so we recommend you book one in advance.\n\nYou can book either a PC or a Mac using BOOKIT.";
		interactionScript[13].url = "http://www1.rmit.edu.au/browse;ID=8p72kxqfvizj";
		#endregion

		#region Books
		interactionScript[14].title = "Where are the books?";
		interactionScript[14].body = "If it's a book you're looking for, follow me. We have over 150,000 items in this Library, with collections on Level 5 and Level 6 which include books, journals and audiovisual material. If an item you need is on loan, or at another Library site, you can place a hold on it.";

		interactionScript[15].title = "Where are the books?";
		interactionScript[15].body = "What you see on the shelves is just the tip of the iceberg. Print resources account for only 20% of the collection and a huge 80% is electronic.\n\nYou can find any of our resources by searching on LibrarySearch.";
		interactionScript[15].url = "http://www1.rmit.edu.au/library";

		// MOBILE ONLY
		interactionScript[16].title = "Tip";
		interactionScript[16].body = "Did you know? Subject guides are brilliant way to discover all the resources you’ll need when doing your assignments.";
		interactionScript[16].url = "http://rmit.libguides.com/";

		// MOBILE ONLY
		interactionScript[17].title = "How do I read a call number?";
		interactionScript[17].body = "Call numbers group similar subjects and topics together on the Library shelves. When you search LibrarySearch you’ll see the call number for the item you want. Learn how to find a book on the shelf, once you have the call number.";
		interactionScript[17].url = "https://www.youtube.com/watch?v=5obnVWICD64&index=4&list=PL58B2ECFB395955F9";
		#endregion

		#region Return Books
		interactionScript[18].title = "Where do I return books?";
		interactionScript[18].body = "When it's time to return your books, here is where you put them.";
		//TODO: add book return x-ray graphic
		#endregion

		#region Quiet Study
		interactionScript[19].title = "Where are places for quiet study?";
		interactionScript[19].body = "Quiet study areas are available on Level 5 near the books. A bit of whispering is okay in this space. If you'd like somewhere really quiet try the silent area on Level 6.";
		#endregion

		#region Reserve Collection
		interactionScript[20].title = "What is the Reserve Collection and how do I find it?";
		interactionScript[20].body = "The Reserve Collection consists of material that is heavily used. It includes books, audiovisual items, photocopies of articles, lecture notes and lecturers' copies of books. You can borrow Reserve items for four hours to use in the Library.  The Reserve collection is located on ...(to be moved soon). That's where you borrow and return the items.";
		#endregion

		#region Place a hold
		interactionScript[21].title = "How do I place a hold?";
		interactionScript[21].body = "If an item you need is on loan, or at another Library site, you can place a hold on it in LibrarySearch. You’ll receive an email when the item is ready for collection from the holds shelf.";
		interactionScript[21].url = "http://www1.rmit.edu.au/library/borrowing/holds";
		#endregion

		#region Toilets
		interactionScript[22].title = "Where are the toilets?";
		interactionScript[22].body = "DIRECTIONS PENDING"; // TODO: correct directions
		#endregion

		#region Group work
		interactionScript[23].title = "Where can I do group work?";
		interactionScript[23].body = "If you'd like to discuss your assignments or plan presentations you can use one of the many meeting and project rooms.\nYou can book a room using BOOKIT on the Library website.";
		interactionScript[23].url = "http://www1.rmit.edu.au/browse;ID=jvei8t7qgnxv1";
		#endregion

		#region Printers
		interactionScript[24].title = "How do I print, copy or scan?";
		interactionScript[24].body = "Need to print out an assignment or do some photocopying? The Library has many devices for printing, copying and scanning. Just top up your print credit on your student card and you'll be ready to go. You can also scan to a USB or directly to email.";
		// TODO: Show printer locations on map?
		interactionScript[24].url = "https://www.rmit.edu.au/students/support-and-facilities/it-services-for-students/printing";
		#endregion

		#region Tour Complete
		interactionScript[25].title = "Tour complete";
		interactionScript[25].body = "Thanks for taking the tour. We look forward to seeing you at Swanston Library and at the Study and Learning Centre Drop-in throughout the year.";

		// HOLOLENS ONLY
		interactionScript[26].title = "Tour complete";
		interactionScript[26].body = "Now it's time to say goodbye to the Hololens and return it to the librarian at the service desk. For more information and for links to all the services we talked about today download the LibGo app.";

		interactionScript[27].title = "Tour complete";
		interactionScript[27].body = "If you enjoyed this tour, please tell your friends. \n\nHappy studying and remember we're here to help you!";
		#endregion

		#region Restart
		interactionScript[28].title = "Restart";
		interactionScript[28].body = "";
		#endregion

		switch (posterNumber) {
			case 1: // Introduction
				if (currentPlatform == Platforms.hololens) {
					posterContent.Add(0);
					posterContent.Add(1);
					posterContent.Add(2);
					posterContent.Add(3);
					posterContent.Add(4);
					posterContent.Add(6);
				} else {
					posterContent.Add(2);
					posterContent.Add(3);
					posterContent.Add(4);
					posterContent.Add(6);
				}
				break;
			case 2: // Study and learning centre
				posterContent.Add(7);
				posterContent.Add(8);
				posterContent.Add(9);
				if (currentPlatform == Platforms.mobile) {
					posterContent.Add(10);
					posterContent.Add(11);
				}
				break;
			case 3: // Computers
				posterContent.Add(14);
				posterContent.Add(15);
				if (currentPlatform == Platforms.mobile) {
					posterContent.Add(16);
					posterContent.Add(17);
				}
				break;
			case 4: // books
				posterContent.Add(18);
				break;
			case 5: // quiet study
				posterContent.Add(19);
				break;
			case 6: // reserve collection and place a hold
				posterContent.Add(20);
				posterContent.Add(21);
				break;
			case 7: // toilets
				posterContent.Add(22);
				break;
			case 8: // group work - printers - tour complete
				posterContent.Add(23);
				posterContent.Add(24);
				posterContent.Add(25);
				if (currentPlatform == Platforms.hololens)
					posterContent.Add(26);
				posterContent.Add(27);
				break;
			case 9: // printers
				posterContent.Add(24);
				break;
			case 10: // tour complete only (trigger this if the user takes too long on Hololens)
				posterContent.Add(25);
				posterContent.Add(26);
				posterContent.Add(27);
				break;
		}
		Debug.Log(posterContent.Count.ToString() + " elements in poster content, starting with number " + posterContent[0].ToString());

		//myCanvas.SetActive(true);
		hexButtons.SetActive(false);
		scriptIndex = 0;
		DisplayContent();
	}

	private void AvatarGesture(Gesture gesture)
	{
		//TODO: play suitable sound effect?
		myAnimator.SetTrigger(gesture.ToString());
	}

	/// <summary>
	/// Displays the next element from the poster list (using the scriptIndex as a counter)
	/// </summary>
	private void DisplayContent()
	{
		Debug.Log("Script index: " + scriptIndex.ToString() + ", displaying script element " + posterContent[scriptIndex].ToString());

		// draw the speech bubble with the interactionScript
		int nextItem = posterContent[scriptIndex];
		speechBubble.DrawSpeechBubble(interactionScript[nextItem].title, interactionScript[nextItem].body);

		// turn off any active trails
		// disabled for demo TODO: re-enable this
		/*
		if (index > 0) {
			foreach (FlashingTrail trail in trails) {
				trail.SetTrailActive(false);
			}
		}*/

		// add any special animation here
		/*
		switch (posterContent[index]) {
			case 0:
				buttonText.text = "Continue"; // reset in case of restart
				//trails[0].SetTrailActive(true); // demo trail
				break;
			case 1: // acknowledgement of country
				AvatarGesture(Gesture.idleLook);
				break;
			case 2: // welcome to the library
				AvatarGesture(Gesture.wave);
				break;
			case 3: // I'm owlbot
				AvatarGesture(Gesture.question);
				break;
			case 4: // opening hours
				AvatarGesture(Gesture.openingHours);
				break;
			case 5: // Where can I get help?
				GameObject.Find("prop_clock").SetActive(false); // trying to prevent items showing up in later animations.
				AvatarGesture(Gesture.helpDesk);
				break;
			//trails[0].SetTrailActive(true);
			case 8: // study and learning center
				AvatarGesture(Gesture.idleFlap);
				break;
			case 10: // Computers
				AvatarGesture(Gesture.laptop);
				//trails[0].SetTrailActive(true);
				break;
			case 12: // Where are the books
				AvatarGesture(Gesture.whereAreTheBooks);
				break;
			case 14: // Where do I return books?
				AvatarGesture(Gesture.returningBooks);
				//trails[1].SetTrailActive(true);
				break;
			case 15: // Reserve Collection
				AvatarGesture(Gesture.pointRight);
				//trails[1].SetTrailActive(true);
				break;
			case 16: // Places for quiet study
				AvatarGesture(Gesture.groupStudy);
				//trails[0].SetTrailActive(true);
				break;
			case 17: // group study
				AvatarGesture(Gesture.groupWork);
				break;
			case 18: // Printing
				AvatarGesture(Gesture.printing);
				break;
			case 21: // goodbye
				AvatarGesture(Gesture.wave);
				break;
			case 22: // finished
				buttonText.text = "Restart";
				scriptIndex = -1; // next button press will restart at 0
				break;
		}*/
	}

	public void PressedButton(int buttonNumber)
	{
		//Debug.Log("Pressed continue at " + Time.time.ToString());
		if (playAudio) {
			owlbotAudio.clip = audioClips[0]; // click
			owlbotAudio.Play();
		}
		switch (buttonNumber) {
			case 1: // continue
				scriptIndex++;
				if (scriptIndex < posterContent.Count)
					DisplayContent();
				else {
					// teleport Owlbot away

					// TODO: tell the user how to get to the next poster or add directions to the end of each poster's content
				}
				break;
		}
	}
}
