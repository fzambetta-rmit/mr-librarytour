﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlashingTrail : MonoBehaviour
{
	public float flashDelay, flashOffset;
	public bool flashing;

	private GameObject[] trailObjects;

	void Start()
	{
		// put all the lights in an array and disable them
		trailObjects = new GameObject[transform.childCount];
		for (int i = 0; i < transform.childCount; i++) {
			trailObjects[i] = transform.GetChild(i).gameObject;
			trailObjects[i].SetActive(false);
		}
	}

	public void SetTrailActive(bool state)
	{
		if (state)
			StartCoroutine(DoActivation());
		else {
			flashing = false;
			for (int i = 0; i < trailObjects.Length; i++) {
				trailObjects[i].SetActive(false);
			}
		}

	}

	private IEnumerator DoActivation()
	{
		flashing = true;
		for (int i = 0; i < trailObjects.Length; i++) {
			trailObjects[i].SetActive(true);
			yield return new WaitForSeconds(flashOffset);
		}

		StartCoroutine(Flash());
	}

	private IEnumerator Flash()
	{
		yield return new WaitForSeconds(flashDelay); // wait between strobes

		for (int i = 0; i < trailObjects.Length; i++) {
			trailObjects[i].SetActive(false);
			yield return new WaitForSeconds(flashOffset);
			if (flashing)
				trailObjects[i].SetActive(true);
		}

		if (flashing)
			StartCoroutine(Flash());
	}

	[ContextMenu("Test trail")]
	private void TestTrail ()
	{
		if (!flashing)
			SetTrailActive(true);
		else
			SetTrailActive(false);
	}
}
