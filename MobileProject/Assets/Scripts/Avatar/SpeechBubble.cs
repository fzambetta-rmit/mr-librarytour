﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class SpeechBubble : MonoBehaviour {

	public TextMeshProUGUI title, body;

	void Start ()
	{
		//gameObject.SetActive(false);	
	}
	
	public void DrawSpeechBubble(string _title, string _body)
	{
		title.text = _title;
		body.text = _body;
		gameObject.SetActive(true);
	}

	public void HideSpeechBubble()
	{
		gameObject.SetActive(false);
	}
}
