﻿using System.Collections;
using System.Collections.Generic;
using HoloToolkit.Unity.InputModule;
using UnityEngine;
using UnityEngine.UI;

public class ChangeText : MonoBehaviour, IInputClickHandler
{
    public Text text;

    public void OnSelect()
    {
        text.text = "Hello";
    }

    public void OnInputClicked(InputClickedEventData eventData)
    {
        text.text = "Hello";
    }
}
