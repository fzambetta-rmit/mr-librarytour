﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RIGLabs.EventManager
{
	public enum Events
	{
		MainMenu,
		ResetBall,
		StartGame,
		EndGame,
		NewGame,
		DropBall,
		JumpBall,
		StartScan,
		StopScan,
		//--------
		TOTAL
	}
}