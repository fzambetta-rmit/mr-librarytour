﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PosterTest : MonoBehaviour {

	public CharacterManager characterManagerPrefab;
	
	void Update () {
		if (Input.GetKeyDown(KeyCode.Alpha1)) {
			CharacterManager newCharacter = Instantiate(characterManagerPrefab, transform.position, Quaternion.identity);
			newCharacter.Initialize(1);
		}
		if (Input.GetKeyDown(KeyCode.Alpha2)) {
			CharacterManager newCharacter = Instantiate(characterManagerPrefab, transform.position, Quaternion.identity);
			newCharacter.Initialize(2);
		}
		if (Input.GetKeyDown(KeyCode.Alpha3)) {
			CharacterManager newCharacter = Instantiate(characterManagerPrefab, transform.position, Quaternion.identity);
			newCharacter.Initialize(3);
		}
		if (Input.GetKeyDown(KeyCode.Alpha4)) {
			CharacterManager newCharacter = Instantiate(characterManagerPrefab, transform.position, Quaternion.identity);
			newCharacter.Initialize(4);
		}
		if (Input.GetKeyDown(KeyCode.Alpha5)) {
			CharacterManager newCharacter = Instantiate(characterManagerPrefab, transform.position, Quaternion.identity);
			newCharacter.Initialize(5);
		}
	}
}
