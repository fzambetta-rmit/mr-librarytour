﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;
using RIGLabs;

namespace RIGLabs.EventManager {
	
	public class EventManager : Singleton<EventManager> {

		private Dictionary <string, UnityEvent> eventDictionary;
		private float floatAmount;

		protected EventManager () {
			Init ();
		}

		void Init()
		{
			if (eventDictionary == null)
			{
				eventDictionary = new Dictionary<string, UnityEvent>();
			}
		}

		void StartListeningInstance (string eventName, UnityAction listener)
		{
			UnityEvent thisEvent = null;
			if (Instance.eventDictionary.TryGetValue (eventName, out thisEvent))
			{
				thisEvent.AddListener (listener);
			} 
			else
			{
				thisEvent = new UnityEvent ();
				thisEvent.AddListener (listener);
				Instance.eventDictionary.Add (eventName, thisEvent);
			}
		}

		void StopListeningInstance (string eventName, UnityAction listener)
		{
			UnityEvent thisEvent = null;
			if (Instance.eventDictionary.TryGetValue (eventName, out thisEvent))
			{
				thisEvent.RemoveListener (listener);
			}
		}

		void TriggerEventInstance (string eventName, float amount = 0f)
		{
			UnityEvent thisEvent = null;
			if (Instance.eventDictionary.TryGetValue (eventName, out thisEvent))
			{
				floatAmount = amount;
				thisEvent.Invoke ();
				floatAmount = 0f;
			}
		}

		public static float GetFloat() {
			return Instance.floatAmount;
		}

		public static void TriggerEvent(Events eventEnum, float amount = 0f)
		{
			TriggerEvent (eventEnum.ToString (), amount);
		}

		public static void TriggerEvent(string eventName, float amount = 0f)
		{
			if (Instance == null) 
				return;
			
			Instance.TriggerEventInstance (eventName, amount);
		}

		public static void StopListening (Events eventEnum, UnityAction listener)
		{
			StopListening (eventEnum.ToString (), listener);
		}

		public static void StopListening (string eventName, UnityAction listener)
		{
			if (Instance == null) 
				return;

			Instance.StopListeningInstance (eventName, listener);
		}

		public static void StartListening (Events eventEnum, UnityAction listener)
		{
			StartListening (eventEnum.ToString (), listener);
		}

		public static void StartListening (string eventName, UnityAction listener)
		{
			if (Instance == null) 
				return;

			Instance.StartListeningInstance (eventName, listener);
		}
	}

}