﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Canvas))]
public class FaceCamera : MonoBehaviour {
	private Transform cameraTransform;
	private Transform canvasTransform;
	private Vector3 originalUp;

	void Awake() {
		Camera mainCamera = GameObject.FindGameObjectWithTag ("MainCamera").GetComponent<Camera>();
		cameraTransform = mainCamera.transform;

		Canvas canvas = GetComponent<Canvas> ();
		canvas.worldCamera = mainCamera;
		canvasTransform = canvas.transform;
		originalUp = canvasTransform.up;
	}

	// Update is called once per frame
	void Update () {
		canvasTransform.forward = -Vector3.ProjectOnPlane ((cameraTransform.position - canvasTransform.position), canvasTransform.parent.up);
	}
}
