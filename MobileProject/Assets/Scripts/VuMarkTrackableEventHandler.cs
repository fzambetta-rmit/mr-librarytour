﻿/*===============================================================================
Copyright (c) 2017 PTC Inc. All Rights Reserved.

Confidential and Proprietary - Protected under copyright and other laws.
Vuforia is a trademark of PTC Inc., registered in the United States and other 
countries.
===============================================================================*/
using UnityEngine;

public class VuMarkTrackableEventHandler : DefaultTrackableEventHandler
{
    #region PROTECTED_METHODS
    public VuMarkHandler handler;
    public Transform sphere;
    public Transform cylinder;
    public Transform cube;
    public TextMesh displayID;

    public string ID;

    protected override void OnTrackingFound()
    {
        
        base.OnTrackingFound();
        string ID = handler.getID();
        Debug.Log(ID);
        displayID.text = ID;
        Animator animator = GetComponentInChildren<Animator>();
        if (animator != null)
        {
            animator.SetTrigger("ShowBlinking");
        }
    }
    protected override void OnTrackingLost()
    {
        displayID.text = "";
    }

        #endregion // PROTECTED_METHODS
    }
