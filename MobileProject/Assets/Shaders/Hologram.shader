// Shader created with Shader Forge v1.38 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.38;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,cgin:,lico:1,lgpr:1,limd:1,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,imps:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:7,dpts:2,wrdp:True,dith:0,atcv:False,rfrpo:True,rfrpn:Refraction,coma:15,ufog:False,aust:True,igpj:False,qofs:0,qpre:2,rntp:3,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,atwp:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False,fsmp:False;n:type:ShaderForge.SFN_Final,id:3138,x:32824,y:32705,varname:node_3138,prsc:2|diff-9718-OUT,spec-3028-A,gloss-6256-OUT,emission-8704-OUT,clip-9679-OUT;n:type:ShaderForge.SFN_Color,id:7241,x:32211,y:32668,ptovrint:False,ptlb:Color,ptin:_Color,varname:node_7241,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:1,c3:1,c4:1;n:type:ShaderForge.SFN_Tex2d,id:3028,x:32211,y:32444,ptovrint:False,ptlb:MainTex,ptin:_MainTex,varname:node_3028,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Multiply,id:9718,x:32414,y:32602,varname:node_9718,prsc:2|A-3028-RGB,B-7241-RGB;n:type:ShaderForge.SFN_FragmentPosition,id:1215,x:30795,y:33193,varname:node_1215,prsc:2;n:type:ShaderForge.SFN_Time,id:8982,x:31091,y:33216,varname:node_8982,prsc:2;n:type:ShaderForge.SFN_ValueProperty,id:3173,x:31091,y:33387,ptovrint:False,ptlb:Frequency1,ptin:_Frequency1,varname:node_3173,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.1;n:type:ShaderForge.SFN_Multiply,id:5270,x:31284,y:33295,varname:node_5270,prsc:2|A-8982-T,B-3173-OUT;n:type:ShaderForge.SFN_Add,id:7914,x:31310,y:33129,varname:node_7914,prsc:2|A-5120-OUT,B-5270-OUT;n:type:ShaderForge.SFN_ValueProperty,id:6256,x:32437,y:32973,ptovrint:False,ptlb:Gloss,ptin:_Gloss,varname:node_6256,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.1;n:type:ShaderForge.SFN_Tex2d,id:3490,x:31746,y:33130,ptovrint:False,ptlb:OpacityLines,ptin:_OpacityLines,varname:node_3490,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False|UVIN-1180-OUT;n:type:ShaderForge.SFN_Append,id:1180,x:31546,y:33094,varname:node_1180,prsc:2|A-2547-OUT,B-7914-OUT;n:type:ShaderForge.SFN_Vector1,id:2547,x:31328,y:33035,varname:node_2547,prsc:2,v1:0.5;n:type:ShaderForge.SFN_ValueProperty,id:5602,x:31091,y:33477,ptovrint:False,ptlb:Frequency2,ptin:_Frequency2,varname:node_5602,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.2;n:type:ShaderForge.SFN_Tex2d,id:9199,x:31746,y:33333,ptovrint:False,ptlb:OapcityLines2,ptin:_OapcityLines2,varname:node_9199,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False|UVIN-5447-OUT;n:type:ShaderForge.SFN_Append,id:5447,x:31572,y:33310,varname:node_5447,prsc:2|A-2547-OUT,B-1644-OUT;n:type:ShaderForge.SFN_Multiply,id:8547,x:31284,y:33459,varname:node_8547,prsc:2|A-8982-T,B-5602-OUT;n:type:ShaderForge.SFN_Add,id:1644,x:31491,y:33459,varname:node_1644,prsc:2|A-5120-OUT,B-8547-OUT;n:type:ShaderForge.SFN_Multiply,id:1241,x:31950,y:33231,cmnt:Scrolling transparent lines,varname:node_1241,prsc:2|A-3490-R,B-9199-R;n:type:ShaderForge.SFN_Noise,id:9067,x:32066,y:33385,varname:node_9067,prsc:2|XY-5910-OUT;n:type:ShaderForge.SFN_Add,id:829,x:32253,y:33307,varname:node_829,prsc:2|A-1241-OUT,B-9067-OUT;n:type:ShaderForge.SFN_Clamp,id:9679,x:32448,y:33322,varname:node_9679,prsc:2|IN-829-OUT,MIN-1883-OUT,MAX-6113-OUT;n:type:ShaderForge.SFN_Vector1,id:1883,x:32253,y:33491,varname:node_1883,prsc:2,v1:-0.1;n:type:ShaderForge.SFN_Vector1,id:6113,x:32253,y:33554,varname:node_6113,prsc:2,v1:1.1;n:type:ShaderForge.SFN_Append,id:5910,x:31869,y:33562,varname:node_5910,prsc:2|A-7914-OUT,B-1644-OUT;n:type:ShaderForge.SFN_ValueProperty,id:3480,x:30795,y:33097,ptovrint:False,ptlb:LineSpacing,ptin:_LineSpacing,varname:node_3480,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.3;n:type:ShaderForge.SFN_Multiply,id:5120,x:30996,y:33084,varname:node_5120,prsc:2|A-3480-OUT,B-1215-Y;n:type:ShaderForge.SFN_Tex2d,id:4190,x:31757,y:32876,ptovrint:False,ptlb:GlowLines,ptin:_GlowLines,varname:node_4190,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False|UVIN-8397-OUT;n:type:ShaderForge.SFN_OneMinus,id:1016,x:32175,y:33087,cmnt:Glow Lines,varname:node_1016,prsc:2|IN-4190-R;n:type:ShaderForge.SFN_ValueProperty,id:5410,x:30982,y:32765,ptovrint:False,ptlb:FrequencyGlow,ptin:_FrequencyGlow,varname:node_5410,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.3;n:type:ShaderForge.SFN_Multiply,id:9631,x:31216,y:32823,varname:node_9631,prsc:2|A-5410-OUT,B-8982-T;n:type:ShaderForge.SFN_Add,id:7897,x:31400,y:32844,varname:node_7897,prsc:2|A-9631-OUT,B-5120-OUT;n:type:ShaderForge.SFN_Append,id:8397,x:31580,y:32857,varname:node_8397,prsc:2|A-2547-OUT,B-7897-OUT;n:type:ShaderForge.SFN_Multiply,id:8704,x:32402,y:33087,varname:node_8704,prsc:2|A-2429-RGB,B-1016-OUT;n:type:ShaderForge.SFN_Color,id:2429,x:32175,y:32915,ptovrint:False,ptlb:GlowColor,ptin:_GlowColor,varname:node_2429,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;proporder:3028-7241-6256-3490-9199-3173-5602-3480-2429-4190-5410;pass:END;sub:END;*/

Shader "Custom/Hologram" {
    Properties {
        _MainTex ("MainTex", 2D) = "white" {}
        _Color ("Color", Color) = (1,1,1,1)
        _Gloss ("Gloss", Float ) = 0.1
        _OpacityLines ("OpacityLines", 2D) = "white" {}
        _OapcityLines2 ("OapcityLines2", 2D) = "white" {}
        _Frequency1 ("Frequency1", Float ) = 0.1
        _Frequency2 ("Frequency2", Float ) = 0.2
        _LineSpacing ("LineSpacing", Float ) = 0.3
        _GlowColor ("GlowColor", Color) = (0.5,0.5,0.5,1)
        _GlowLines ("GlowLines", 2D) = "white" {}
        _FrequencyGlow ("FrequencyGlow", Float ) = 0.3
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "Queue"="AlphaTest"
            "RenderType"="TransparentCutout"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend One OneMinusSrcAlpha
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform float4 _LightColor0;
            uniform float4 _Color;
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform float _Frequency1;
            uniform float _Gloss;
            uniform sampler2D _OpacityLines; uniform float4 _OpacityLines_ST;
            uniform float _Frequency2;
            uniform sampler2D _OapcityLines2; uniform float4 _OapcityLines2_ST;
            uniform float _LineSpacing;
            uniform sampler2D _GlowLines; uniform float4 _GlowLines_ST;
            uniform float _FrequencyGlow;
            uniform float4 _GlowColor;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                LIGHTING_COORDS(3,4)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = UnityObjectToClipPos( v.vertex );
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
                float node_2547 = 0.5;
                float node_5120 = (_LineSpacing*i.posWorld.g);
                float4 node_8982 = _Time;
                float node_7914 = (node_5120+(node_8982.g*_Frequency1));
                float2 node_1180 = float2(node_2547,node_7914);
                float4 _OpacityLines_var = tex2D(_OpacityLines,TRANSFORM_TEX(node_1180, _OpacityLines));
                float node_1644 = (node_5120+(node_8982.g*_Frequency2));
                float2 node_5447 = float2(node_2547,node_1644);
                float4 _OapcityLines2_var = tex2D(_OapcityLines2,TRANSFORM_TEX(node_5447, _OapcityLines2));
                float2 node_5910 = float2(node_7914,node_1644);
                float2 node_9067_skew = node_5910 + 0.2127+node_5910.x*0.3713*node_5910.y;
                float2 node_9067_rnd = 4.789*sin(489.123*(node_9067_skew));
                float node_9067 = frac(node_9067_rnd.x*node_9067_rnd.y*(1+node_9067_skew.x));
                clip(clamp(((_OpacityLines_var.r*_OapcityLines2_var.r)+node_9067),(-0.1),1.1) - 0.5);
                float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
                float3 lightColor = _LightColor0.rgb;
                float3 halfDirection = normalize(viewDirection+lightDirection);
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float3 attenColor = attenuation * _LightColor0.xyz;
///////// Gloss:
                float gloss = _Gloss;
                float specPow = exp2( gloss * 10.0 + 1.0 );
////// Specular:
                float NdotL = saturate(dot( normalDirection, lightDirection ));
                float4 _MainTex_var = tex2D(_MainTex,TRANSFORM_TEX(i.uv0, _MainTex));
                float3 specularColor = float3(_MainTex_var.a,_MainTex_var.a,_MainTex_var.a);
                float3 directSpecular = attenColor * pow(max(0,dot(halfDirection,normalDirection)),specPow)*specularColor;
                float3 specular = directSpecular;
/////// Diffuse:
                NdotL = max(0.0,dot( normalDirection, lightDirection ));
                float3 directDiffuse = max( 0.0, NdotL) * attenColor;
                float3 indirectDiffuse = float3(0,0,0);
                indirectDiffuse += UNITY_LIGHTMODEL_AMBIENT.rgb; // Ambient Light
                float3 diffuseColor = (_MainTex_var.rgb*_Color.rgb);
                float3 diffuse = (directDiffuse + indirectDiffuse) * diffuseColor;
////// Emissive:
                float2 node_8397 = float2(node_2547,((_FrequencyGlow*node_8982.g)+node_5120));
                float4 _GlowLines_var = tex2D(_GlowLines,TRANSFORM_TEX(node_8397, _GlowLines));
                float3 emissive = (_GlowColor.rgb*(1.0 - _GlowLines_var.r));
/// Final Color:
                float3 finalColor = diffuse + specular + emissive;
                return fixed4(finalColor,1);
            }
            ENDCG
        }
        Pass {
            Name "FORWARD_DELTA"
            Tags {
                "LightMode"="ForwardAdd"
            }
            Blend One One
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDADD
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #pragma multi_compile_fwdadd_fullshadows
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform float4 _LightColor0;
            uniform float4 _Color;
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform float _Frequency1;
            uniform float _Gloss;
            uniform sampler2D _OpacityLines; uniform float4 _OpacityLines_ST;
            uniform float _Frequency2;
            uniform sampler2D _OapcityLines2; uniform float4 _OapcityLines2_ST;
            uniform float _LineSpacing;
            uniform sampler2D _GlowLines; uniform float4 _GlowLines_ST;
            uniform float _FrequencyGlow;
            uniform float4 _GlowColor;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                LIGHTING_COORDS(3,4)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = UnityObjectToClipPos( v.vertex );
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
                float node_2547 = 0.5;
                float node_5120 = (_LineSpacing*i.posWorld.g);
                float4 node_8982 = _Time;
                float node_7914 = (node_5120+(node_8982.g*_Frequency1));
                float2 node_1180 = float2(node_2547,node_7914);
                float4 _OpacityLines_var = tex2D(_OpacityLines,TRANSFORM_TEX(node_1180, _OpacityLines));
                float node_1644 = (node_5120+(node_8982.g*_Frequency2));
                float2 node_5447 = float2(node_2547,node_1644);
                float4 _OapcityLines2_var = tex2D(_OapcityLines2,TRANSFORM_TEX(node_5447, _OapcityLines2));
                float2 node_5910 = float2(node_7914,node_1644);
                float2 node_9067_skew = node_5910 + 0.2127+node_5910.x*0.3713*node_5910.y;
                float2 node_9067_rnd = 4.789*sin(489.123*(node_9067_skew));
                float node_9067 = frac(node_9067_rnd.x*node_9067_rnd.y*(1+node_9067_skew.x));
                clip(clamp(((_OpacityLines_var.r*_OapcityLines2_var.r)+node_9067),(-0.1),1.1) - 0.5);
                float3 lightDirection = normalize(lerp(_WorldSpaceLightPos0.xyz, _WorldSpaceLightPos0.xyz - i.posWorld.xyz,_WorldSpaceLightPos0.w));
                float3 lightColor = _LightColor0.rgb;
                float3 halfDirection = normalize(viewDirection+lightDirection);
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float3 attenColor = attenuation * _LightColor0.xyz;
///////// Gloss:
                float gloss = _Gloss;
                float specPow = exp2( gloss * 10.0 + 1.0 );
////// Specular:
                float NdotL = saturate(dot( normalDirection, lightDirection ));
                float4 _MainTex_var = tex2D(_MainTex,TRANSFORM_TEX(i.uv0, _MainTex));
                float3 specularColor = float3(_MainTex_var.a,_MainTex_var.a,_MainTex_var.a);
                float3 directSpecular = attenColor * pow(max(0,dot(halfDirection,normalDirection)),specPow)*specularColor;
                float3 specular = directSpecular;
/////// Diffuse:
                NdotL = max(0.0,dot( normalDirection, lightDirection ));
                float3 directDiffuse = max( 0.0, NdotL) * attenColor;
                float3 diffuseColor = (_MainTex_var.rgb*_Color.rgb);
                float3 diffuse = directDiffuse * diffuseColor;
/// Final Color:
                float3 finalColor = diffuse + specular;
                return fixed4(finalColor * 1,0);
            }
            ENDCG
        }
        Pass {
            Name "ShadowCaster"
            Tags {
                "LightMode"="ShadowCaster"
            }
            Offset 1, 1
            Cull Back
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_SHADOWCASTER
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform float _Frequency1;
            uniform sampler2D _OpacityLines; uniform float4 _OpacityLines_ST;
            uniform float _Frequency2;
            uniform sampler2D _OapcityLines2; uniform float4 _OapcityLines2_ST;
            uniform float _LineSpacing;
            struct VertexInput {
                float4 vertex : POSITION;
            };
            struct VertexOutput {
                V2F_SHADOW_CASTER;
                float4 posWorld : TEXCOORD1;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = UnityObjectToClipPos( v.vertex );
                TRANSFER_SHADOW_CASTER(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                float node_2547 = 0.5;
                float node_5120 = (_LineSpacing*i.posWorld.g);
                float4 node_8982 = _Time;
                float node_7914 = (node_5120+(node_8982.g*_Frequency1));
                float2 node_1180 = float2(node_2547,node_7914);
                float4 _OpacityLines_var = tex2D(_OpacityLines,TRANSFORM_TEX(node_1180, _OpacityLines));
                float node_1644 = (node_5120+(node_8982.g*_Frequency2));
                float2 node_5447 = float2(node_2547,node_1644);
                float4 _OapcityLines2_var = tex2D(_OapcityLines2,TRANSFORM_TEX(node_5447, _OapcityLines2));
                float2 node_5910 = float2(node_7914,node_1644);
                float2 node_9067_skew = node_5910 + 0.2127+node_5910.x*0.3713*node_5910.y;
                float2 node_9067_rnd = 4.789*sin(489.123*(node_9067_skew));
                float node_9067 = frac(node_9067_rnd.x*node_9067_rnd.y*(1+node_9067_skew.x));
                clip(clamp(((_OpacityLines_var.r*_OapcityLines2_var.r)+node_9067),(-0.1),1.1) - 0.5);
                SHADOW_CASTER_FRAGMENT(i)
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
